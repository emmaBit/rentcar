﻿using RentCar.Model.Enums;
using RentCar.Model.Extensions;
using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class Cliente : IBaseEntity
    {
        public Cliente()
        {
            Inspecciones = new HashSet<Inspeccion>();
            Rentas = new HashSet<Renta>();
        }
        [Column("ClienteId")]
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string Estado { get; set; }
        public string NoTarjetaCR { get; set; }
        public decimal LimiteCredito { get; set; }
        public string TipoPersona { get; set; } //Fisica, Juridica
        public virtual ICollection<Inspeccion> Inspecciones { get; set; }
        public virtual ICollection<Renta> Rentas { get; set; }

    }
}
