﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentCar.Model.Models
{
    public class Empleado : IBaseEntity
    {
        public Empleado()
        {
            Inspecciones = new HashSet<Inspeccion>();
            Rentas = new HashSet<Renta>();
        }
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string Estado { get; set; }
        public int PorcientoComision { get; set; }
        public string TandaLabor { get; set; }
        public virtual ICollection<Inspeccion> Inspecciones { get; set; }
        public virtual ICollection<Renta> Rentas { get; set; }
    }
}
