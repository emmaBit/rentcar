﻿using RentCar.Model.Enums;
using RentCar.Model.Extensions;
using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class Inspeccion : IBaseEntity
    {
        [Column("InspeccionId")]
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int VehiculoId { get; set; }
        public int EmpleadoId { get; set; }
        public DateTime FechaInspeccion { get; set; }

        public bool TieneGato { get; set; }
        public bool TieneRalladuras { get; set; }

        public string CantidadCombustible { get; set; }

        public string EstadoGomaDelanteraIzq { get; set; } //Bueno, Regular, Malo, Otro
        public string EstadoGomaDelanteraDer { get; set; }
        public string EstadoGomaTraseraDer { get; set; }
        public string EstadoGomaTraseraIzq { get; set; }
        public string EstadoGomaRepuesta { get; set; }

        public string EstadoCristalDelanteroIzq { get; set; }
        public string EstadoCristalDelanteroDer { get; set; }
        public string EstadoCristalTraseroIzq { get; set; }
        public string EstadoCristalTraseroDer { get; set; }
        public string EstadoCristalFrontal { get; set; }
        public string EstadoCristalTrasero { get; set; }
        public string Estado { get; set; }
        public string Descripcion { get; set; }

        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual Vehiculo Vehiculo { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Empleado Empleado { get; set; }
    }
}
