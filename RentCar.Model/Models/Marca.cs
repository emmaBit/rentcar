﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class Marca : IBaseEntity
    {
        public Marca()
        {
            Modelos = new HashSet<Modelo>();
        }
        [Column("MarcaId")]
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public string Estado { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime CreatedDate { get; set; }
        public ICollection<Modelo> Modelos { get; set; }
    }
}
