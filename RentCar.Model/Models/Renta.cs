﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class Renta : IBaseEntity
    {
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ClienteId { get; set; }
        public int VehiculoId { get; set; }
        public int EmpleadoId { get; set; }
        public decimal MontoPorDia { get; set; }
        public int CantidadDia { get; set; }
       
        public string Comentario { get; set; }
        public string Estado { get; set; }
        public string Leandro { get; set; }
        public DateTime FechaRenta { get; set; }
        public DateTime? FechaDevolucion { get; set; }
        public virtual Vehiculo Vehiculo { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Empleado Empleado { get; set; }

        [NotMapped]
        public decimal Total
        {
            get
            {
                return MontoPorDia * CantidadDia;
            }
        }
       
    }
}
