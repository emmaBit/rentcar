﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentCar.Model.Generic
{
    public interface IBaseEntity
    {
        int Id { get; set; }
        bool Deleted { get; set; }
        DateTime CreatedDate { get; set; }
    }
}
