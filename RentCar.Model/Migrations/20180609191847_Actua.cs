﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentCar.Model.Migrations
{
    public partial class Actua : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "TipoVehiculo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "TipoCombustible",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "Modelo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "Marca",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "TipoVehiculo");

            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "TipoCombustible");

            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "Modelo");

            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "Marca");
        }
    }
}
