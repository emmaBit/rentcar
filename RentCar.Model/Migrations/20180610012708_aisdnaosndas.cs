﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentCar.Model.Migrations
{
    public partial class aisdnaosndas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehiculo_TipoVehiculo_TipoVehiculoId",
                table: "Vehiculo");

            migrationBuilder.AlterColumn<int>(
                name: "TipoVehiculoId",
                table: "Vehiculo",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "Vehiculo",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehiculo_TipoVehiculo_TipoVehiculoId",
                table: "Vehiculo",
                column: "TipoVehiculoId",
                principalTable: "TipoVehiculo",
                principalColumn: "TipoVehiculoId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehiculo_TipoVehiculo_TipoVehiculoId",
                table: "Vehiculo");

            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "Vehiculo");

            migrationBuilder.AlterColumn<int>(
                name: "TipoVehiculoId",
                table: "Vehiculo",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Vehiculo_TipoVehiculo_TipoVehiculoId",
                table: "Vehiculo",
                column: "TipoVehiculoId",
                principalTable: "TipoVehiculo",
                principalColumn: "TipoVehiculoId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
