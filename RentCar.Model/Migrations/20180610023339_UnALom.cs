﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentCar.Model.Migrations
{
    public partial class UnALom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "LimiteCredito",
                table: "Cliente",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "LimiteCredito",
                table: "Cliente",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
