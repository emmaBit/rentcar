﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentCar.Model.Migrations
{
    public partial class UnALomdasdasddasdasd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "MontoPorDia",
                table: "Renta",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "MontoPorDia",
                table: "Renta",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
