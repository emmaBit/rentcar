﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentCar.Model.Migrations
{
    public partial class UnALomdasdasd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LugaresRalladuras",
                table: "Inspeccion",
                newName: "Estado");

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "Inspeccion",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "Inspeccion");

            migrationBuilder.RenameColumn(
                name: "Estado",
                table: "Inspeccion",
                newName: "LugaresRalladuras");
        }
    }
}
