﻿
namespace RentCar.Model.Helpers
{ 
    public class DescriptionValue
    {
        public string Description { get; set; }
        public int Value { get; set; }

    }
}
