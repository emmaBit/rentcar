﻿using System.ComponentModel;

namespace RentCar.Model.Enums
{
    public enum EstadoCristal
    {
        [Description("Bueno")]
        Bueno = 0,
        [Description("Roto")]
        Roto = 1,
        [Description("Rallado")]
        Rallado = 2,
        [Description("No existe")]
        NoExiste = 3
    }
}
