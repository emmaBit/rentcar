﻿using RentCar.Model.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace RentCar.Model.Extensions
{
    public static class Extensions
    {
        public static string ToNumberCedula(this string value)
        {
            if (value == null)
                return null;

            var result = value.Split('-');
            var prefijo = $"{result[0]}{result[1]}{result[2]}";
            return prefijo;
        }

        public static string ToLocalCedula(this string value)
        {
            if (value == null)
                return null;

            var prefijo = string.Concat(value.Substring(0, 3), "-");
            var medio = string.Concat(value.Substring(3, 7), "-");
            var result = string.Concat(string.Concat(prefijo, medio), value.LastOrDefault());
            return result;
        }

        public static string GetNameEnum(this Enum eff)
        {
            return Enum.GetName(eff.GetType(), eff);
        }
        public static string GetDescriptionEnum<T>(this Enum eff)
        {
            var name = Enum.GetName(eff.GetType(), eff);
            return GetDescriptionFromEnum<T>(name);
        }
        public static EnumType GetEnum<EnumType>(this string enumValue)
        {
            return (EnumType) Enum.Parse(typeof(EnumType), enumValue);
        }

        public static string GetDescriptionFromEnum<EnumType>(this string name)
        {
            //get the member info of the enum
            MemberInfo[] memberInfos = typeof(EnumType).GetMembers();
            if (memberInfos.Length > 0)
            {
                //loop through the member info classes
                foreach (var attributes in from memberInfo in memberInfos
                                           let attributes =
                                           memberInfo.GetCustomAttributes(typeof(DescriptionAttribute), false)
                                           where attributes.Length > 0
                                           where memberInfo.Name == name
                                           select attributes)
                {
                    var descriptionAttribute = (DescriptionAttribute)attributes.FirstOrDefault();
                    if (descriptionAttribute != null)
                        return descriptionAttribute.Description;
                }
            }

            //this means the enum was not found from the description, so return the default
            return String.Empty;
        }

        public static IEnumerable<DescriptionValue> ToListOptions<TEnum>(this TEnum enumeracion)
        {
            var enumType = typeof(TEnum);
            var values = Enum.GetNames(enumType)
            .Select(enumerator => new
            {
                Descripcion = enumerator.GetDescriptionFromEnum<TEnum>(),
                Real = enumerator
            }).ToList();

            var items = new List<DescriptionValue>();
            for (int i = 0; i < values.Count; i++)
            {
                items.Add(new DescriptionValue
                {
                    Description = values[i].Descripcion.ToString(CultureInfo.InvariantCulture),
                    Value = (int)Enum.Parse(typeof(TEnum), values[i].Real)
                });
            }


            return items;
        }
    }
}
