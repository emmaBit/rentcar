﻿using Microsoft.EntityFrameworkCore;
using RentCar.Model.Generic;
using RentCar.Model.Models;
using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace RentCar.Model
{
    public interface IRentCarContext
    {
        DbSet<Cliente> Cliente { get; set; }
        DbSet<Empleado> Empleado { get; set; }
        DbSet<Inspeccion> Inspeccion { get; set; }
        DbSet<Marca> Marca { get; set; }
        DbSet<Modelo> Modelo { get; set; }
        DbSet<Renta> Renta { get; set; }
        DbSet<TipoCombustible> TipoCombustible { get; set; }
        DbSet<TipoVehiculo> TipoVehiculo { get; set; }
        DbSet<Vehiculo> Vehiculo { get; set; }
        DbSet<T> GetDbSet<T>() where T : class, IBaseEntity;
        int SaveChanges(bool acceptAllChangesOnSuccess);
        void Dispose(bool disposing);
    }
    
    public class RentCarContext : DbContext, IRentCarContext
    {
        public RentCarContext()
        {
        }

        public RentCarContext(DbContextOptions<RentCarContext> options)
            : base(options)
        {

        }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Empleado> Empleado { get; set; }
        public DbSet<Inspeccion> Inspeccion { get; set; }
        public DbSet<Marca> Marca { get; set; }
        public DbSet<Modelo> Modelo { get; set; }
        public DbSet<Renta> Renta { get; set; }
        public DbSet<TipoCombustible> TipoCombustible { get; set; }
        public DbSet<TipoVehiculo> TipoVehiculo { get; set; }
        public DbSet<Vehiculo> Vehiculo { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = ConfigurationManager.AppSettings["connectionString"];
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var type in modelBuilder.Model.GetEntityTypes())
            {
                if (typeof(IBaseEntity).IsAssignableFrom(type.ClrType))
                    modelBuilder.SetSoftDeleteFilter(type.ClrType);
            }
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return base.SaveChangesAsync();
        }

        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries<IBaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["Deleted"] = false;
                        entry.CurrentValues["CreatedDate"] = DateTime.Now;
                        break;

                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["Deleted"] = true;
                        break;
                }
            }
        }

        public DbSet<T> GetDbSet<T>() where T : class, IBaseEntity
        {
            return Set<T>();
        }

        public void Dispose(bool disposing)
        {
            base.Dispose();
        }
    }
}
