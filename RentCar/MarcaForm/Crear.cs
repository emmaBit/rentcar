﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using System.Windows.Forms;

namespace RentCar.MarcaForm
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Marca model;
        private Marca _editingEntity;
        private BaseRespository<Marca> _db;
        public Crear()
        {
            model = new Marca();
            _db = new BaseRespository<Marca>();
            InitializeComponent();

        }
        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }


        public Crear(Marca editingEntity)
        {
            _editingEntity = editingEntity;
            _db = new BaseRespository<Marca>();
            InitializeComponent();
            txtNombre.Text = _editingEntity.Nombre;
            txtDescripcion.Text = _editingEntity.Descripcion;
        }

        void Save()
        {
            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (_editingEntity != null)
            {
                _editingEntity.Nombre = txtNombre.Text;
                _editingEntity.Descripcion = txtDescripcion.Text;
                _db.Update(_editingEntity);
            }
            else
            {
                model.Nombre = txtNombre.Text;
                model.Descripcion = txtDescripcion.Text;

                model.Estado = "Activo";
                _db.Add(model);
            }
            _db.SaveChanges();
            Limpiar();
            Regresar();

        }
        void Limpiar()
        {
            model = new Marca();
            txtNombre.Text = model.Nombre;
            txtDescripcion.Text = model.Descripcion;
        }
        void Regresar()
        {
            Visible = false;
            new Lista
            {
                Visible = true
            };
        }

        bool ModelIsValid()
        {
            if (txtNombre.Text == null || txtNombre.Text == "")
                return false;

            return true;
        }
    }
}
