﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RentCar.Model.Models;
using RentCar.BL.Repositories;

namespace RentCar.MarcaForm
{
    public partial class Lista : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private IBaseRespository<Marca> _db;
        private ViewModel _modelSelected;
        public Lista()
        {
            _db = new BaseRespository<Marca>();
            InitializeComponent();
            LoadData();
        }

        public BindingList<ViewModel> GetDataSource()
        {
            BindingList<ViewModel> result = new BindingList<ViewModel>();

            foreach (var item in _db.GetAll())
            {
                result.Add(new ViewModel
                {
                    Id = item.Id,
                    CreatedDate = item.CreatedDate,
                    Estado = item.Estado,
                    Descripcion = item.Descripcion,
                    Nombre = item.Nombre
                });
            }
            return result;
        }
        private void LoadData()
        {
            BindingList<ViewModel> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "Resgistros : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Visible = false;

            var nuevoMarca = new Crear
            {
                Visible = true
            };
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            _modelSelected = (ViewModel)row.GetFocusedRow();
        }

        private void btnEditar_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                Visible = false;
                var _editingModel = new Marca
                {
                    Id = _modelSelected.Id,
                    Nombre = _modelSelected.Nombre,
                    Descripcion = _modelSelected.Descripcion,
                    Estado = _modelSelected.Estado,
                    CreatedDate = _modelSelected.CreatedDate
                };
                new Crear(_editingModel).Visible = true;
            }

        }

        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                var result = MessageBox.Show("¿Borrar este registro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    _db.Remove(_modelSelected.Id);
                    _db.SaveChanges();
                    LoadData();
                }
            }
        }
    }

    public class ViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Estado { get; set; }
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
    }
}
