﻿namespace RentCar.MarcaForm
{
    partial class Crear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtDescripcion = new System.Windows.Forms.RichTextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.paneLeftBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panelBottomBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtDescripcion);
            this.panelControl1.Controls.Add(this.txtNombre);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(52, 43);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(816, 333);
            this.panelControl1.TabIndex = 10;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(212, 111);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(590, 169);
            this.txtDescripcion.TabIndex = 7;
            this.txtDescripcion.Text = "";
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(212, 27);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(590, 40);
            this.txtNombre.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(62, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 34);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 34);
            this.label1.TabIndex = 4;
            this.label1.Text = "Descripción:";
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.Location = new System.Drawing.Point(52, 0);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(12, 6, 0, 0);
            this.labelControl1.Size = new System.Drawing.Size(816, 43);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Marca";
            // 
            // paneLeftBtn
            // 
            this.paneLeftBtn.BackColor = System.Drawing.Color.White;
            this.paneLeftBtn.ButtonInterval = 0;
            windowsUIButtonImageOptions1.ImageUri.Uri = "Backward;Size32x32;GrayScaled";
            this.paneLeftBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Regresar", -1, false)});
            this.paneLeftBtn.ContentAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.paneLeftBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.paneLeftBtn.ForeColor = System.Drawing.Color.Gray;
            this.paneLeftBtn.Location = new System.Drawing.Point(0, 0);
            this.paneLeftBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.paneLeftBtn.MaximumSize = new System.Drawing.Size(52, 0);
            this.paneLeftBtn.MinimumSize = new System.Drawing.Size(52, 0);
            this.paneLeftBtn.Name = "paneLeftBtn";
            this.paneLeftBtn.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.paneLeftBtn.Padding = new System.Windows.Forms.Padding(6, 6, 0, 0);
            this.paneLeftBtn.Size = new System.Drawing.Size(52, 376);
            this.paneLeftBtn.TabIndex = 7;
            this.paneLeftBtn.Text = "windowsUIButtonPanel1";
            this.paneLeftBtn.UseButtonBackgroundImages = false;
            this.paneLeftBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelLeftBtnClick);
            // 
            // panelBottomBtn
            // 
            this.panelBottomBtn.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.panelBottomBtn.AppearanceButton.Normal.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Normal.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.panelBottomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            windowsUIButtonImageOptions2.ImageUri.Uri = "Save;Size32x32;GrayScaled";
            windowsUIButtonImageOptions3.ImageUri.Uri = "Reset;Size32x32;GrayScaled";
            this.panelBottomBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Guardar", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Guardar", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Limpiar", -1, false)});
            this.panelBottomBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottomBtn.EnableImageTransparency = true;
            this.panelBottomBtn.ForeColor = System.Drawing.Color.White;
            this.panelBottomBtn.Location = new System.Drawing.Point(0, 376);
            this.panelBottomBtn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panelBottomBtn.MaximumSize = new System.Drawing.Size(0, 74);
            this.panelBottomBtn.MinimumSize = new System.Drawing.Size(70, 74);
            this.panelBottomBtn.Name = "panelBottomBtn";
            this.panelBottomBtn.Size = new System.Drawing.Size(868, 74);
            this.panelBottomBtn.TabIndex = 8;
            this.panelBottomBtn.Text = "windowsUIButtonPanelMain";
            this.panelBottomBtn.UseButtonBackgroundImages = false;
            this.panelBottomBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelBottomBtnClick);
            // 
            // Crear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 450);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.paneLeftBtn);
            this.Controls.Add(this.panelBottomBtn);
            this.Name = "Crear";
            this.Text = "Crear";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.RichTextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel paneLeftBtn;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel panelBottomBtn;
    }
}