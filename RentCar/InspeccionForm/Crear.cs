﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using RentCar.Model;
using RentCar.Model.Generic;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity;
using System.Windows.Forms;

namespace RentCar.InspeccionForm
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Inspeccion model;
        private BaseRespository<Inspeccion> _db;
        private IRentCarContext _ctx;
        private Renta _renta;
        public Crear()
        {
            model = new Inspeccion();
            _db = new BaseRespository<Inspeccion>();
            _ctx = new RentCarContext();
            InitializeComponent();
            Init();
        }

        public Crear(int rentaId)
        {
            model = new Inspeccion();
            _db = new BaseRespository<Inspeccion>();
            _ctx = new RentCarContext();
            _renta = _ctx.Renta
                .Include(x => x.Cliente)
                .Include(x => x.Vehiculo)
                .Include(x => x.Empleado)
                .FirstOrDefault(x => x.Id == rentaId);
            
            InitializeComponent();
            Init();
            model.VehiculoId = _renta.VehiculoId;
            cbbCliente.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_renta.Cliente.Nombre, value: _renta.ClienteId);
            cbbEmpleado.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_renta.Empleado.Nombre, value: _renta.EmpleadoId);
            cbbVehiculo.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_renta.Vehiculo.NoPlaca, value: _renta.VehiculoId);
            cbbVehiculo.Enabled = false;
        }
        private void LoadCbbVehiculo()
        {
            var items = cbbVehiculo.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (var record in _ctx.Vehiculo.ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(record.NoPlaca, value: record.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }
        private void LoadCbbCombustible()
        {
            cbbCombustible.Properties.Items.Add("1/4");
            cbbCombustible.Properties.Items.Add("1/2");
            cbbCombustible.Properties.Items.Add("3/4");
            cbbCombustible.Properties.Items.Add("LLeno");
        }
        private void LoadCbbGomas(DevExpress.XtraEditors.ComboBoxEdit cbb)
        {
            cbb.Properties.Items.Add("Buena");
            cbb.Properties.Items.Add("Regular");
            cbb.Properties.Items.Add("Mala");
            cbb.Properties.Items.Add("No existe");
        }
        private void LoadCbbCristales(DevExpress.XtraEditors.ComboBoxEdit cbb)
        {
            cbb.Properties.Items.Add("Bueno");
            cbb.Properties.Items.Add("Rallado");
            cbb.Properties.Items.Add("Roto");
            cbb.Properties.Items.Add("No existe");
        }
        private void LoadCbbKyeVaue(DevExpress.XtraEditors.ImageComboBoxEdit cbb, IEnumerable<IBaseEntity> list)
        {
            var items = cbb.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (dynamic record in list.ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(record.Nombre, value: record.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }

        void Init()
        {
            LoadCbbKyeVaue(cbbCliente, _ctx.Cliente);
            LoadCbbKyeVaue(cbbEmpleado, _ctx.Empleado);
            LoadCbbVehiculo();
            LoadCbbCombustible();
            LoadCbbGomas(cbbGomaFroIzq);
            LoadCbbGomas(cbbGomaFroDer);
            LoadCbbGomas(cbbGomaTraIzq);
            LoadCbbGomas(cbbGomaTraDer);
            LoadCbbGomas(cbbGomaRep);
            LoadCbbCristales(cbbCristalFro);
            LoadCbbCristales(cbbCristalFroDer);
            LoadCbbCristales(cbbCristalFroIzq);
            LoadCbbCristales(cbbCristalTra);
            LoadCbbCristales(cbbCristalTraDer);
            LoadCbbCristales(cbbCristalTraIzq);
        }
        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                  //  Regresar();
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }
        void Save()
        {
            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (_renta != null)
            {
                model.Estado = "Inspeccionado para renta";
            }
            else {
                model.Estado = "Inspeccionado para devolución";
            }
            model.FechaInspeccion = DateTime.Now;
            model.CantidadCombustible = cbbCombustible.EditValue?.ToString();
            model.TieneGato = checkGato.Checked;
            model.TieneRalladuras = checkRalladuras.Checked;
            model.Descripcion = txtDescripcion.Text;

            model.EstadoGomaDelanteraDer = cbbGomaFroDer.EditValue?.ToString();
            model.EstadoGomaDelanteraIzq = cbbGomaFroIzq.EditValue?.ToString();
            model.EstadoGomaRepuesta = cbbGomaRep.EditValue?.ToString();
            model.EstadoGomaTraseraDer = cbbGomaTraDer.EditValue?.ToString();
            model.EstadoGomaTraseraIzq = cbbGomaTraIzq.EditValue?.ToString();

            model.EstadoCristalFrontal = cbbCristalFro.EditValue?.ToString();
            model.EstadoCristalDelanteroDer = cbbCristalFroDer.EditValue?.ToString();
            model.EstadoCristalDelanteroIzq = cbbCristalFroIzq.EditValue?.ToString();
            model.EstadoCristalTrasero = cbbCristalTra.EditValue?.ToString();
            model.EstadoCristalTraseroDer = cbbCristalTraDer.EditValue?.ToString();
            model.EstadoCristalTraseroIzq = cbbCristalTraIzq.EditValue?.ToString();

            var result = _db.Add(model);
            _db.SaveChanges();
            if (_renta == null)
            {
                CallRentCar(result.Id);
            }
            else
            {
                CallDevolucion(result.Id);
            }
            Limpiar();
        }

        private void CallRentCar(int inspeccionId)
        {
            Visible = false;
            new Home.Crear(inspeccionId).Visible = true;          
        }
        private void CallDevolucion(int inspeccionId)
        {
            Visible = false;
            new Home.Crear(_renta).Visible = true;
        }
        void Limpiar()
        {
            model = new Inspeccion();
            cbbCombustible.EditValue = null;
            checkGato.Checked = false;
            checkRalladuras.Checked = false;
            txtDescripcion.ResetText();

            cbbGomaFroDer.EditValue = null;
            cbbGomaFroIzq.EditValue = null;
            cbbGomaRep.EditValue = null;
            cbbGomaTraDer.EditValue = null;
            cbbGomaTraIzq.EditValue = null;

            cbbCristalFro.EditValue = null;
            cbbCristalFroDer.EditValue = null;
            cbbCristalFroIzq.EditValue = null;
            cbbCristalTra.EditValue = null;
            cbbCristalTraDer.EditValue = null;
            cbbCristalTraIzq.EditValue = null;

            cbbCliente.EditValue = null;
            cbbVehiculo.EditValue = null;
            cbbEmpleado.EditValue = null;
        }
        void Regresar()
        {
            Visible = false;
            new Lista
            {
                Visible = true
            };
        }

        private void cbbCliente_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if (selected != null) {
                    Int32.TryParse(selected.ToString(), out int id);
                    model.ClienteId = id;
                }
            }
        }

        private void cbbEmpleado_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if (selected != null)
                {
                    Int32.TryParse(selected.ToString(), out int id);
                    model.EmpleadoId = id;
                }
            }
        }

        private void cbbVehiculo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if (selected != null)
                {
                    Int32.TryParse(selected.ToString(), out int id);
                    model.VehiculoId = id;
                }
            }
        }

        bool ModelIsValid()
        {
            if (cbbCliente.Text == null || cbbCliente.Text == "" ||
              cbbEmpleado.Text == null || cbbEmpleado.Text == "" ||
              cbbVehiculo.Text == null || cbbVehiculo.Text == "" ||
              cbbCombustible.Text == null || cbbCombustible.Text == "" ||

              cbbGomaFroDer.Text == null || cbbGomaFroDer.Text == "" ||
              cbbGomaFroIzq.Text == null || cbbGomaFroIzq.Text == "" ||
              cbbGomaRep.Text == null || cbbGomaRep.Text == "" ||
              cbbGomaTraDer.Text == null || cbbGomaTraDer.Text == "" ||
              cbbGomaTraIzq.Text == null || cbbGomaTraIzq.Text == "" ||

              cbbCristalFro.Text == null || cbbCristalFro.Text == "" ||
              cbbCristalFroDer.Text == null || cbbCristalFroDer.Text == "" ||
              cbbCristalFroIzq.Text == null || cbbCristalFroIzq.Text == "" ||
              cbbCristalTra.Text == null || cbbCristalTra.Text == "" ||
              cbbCristalTraIzq.Text == null || cbbCristalTraIzq.Text == "" ||
              cbbCristalTraDer.Text == null || cbbCristalTraDer.Text == "" 
             )
                return false;

            return true;
        }
    }
}
