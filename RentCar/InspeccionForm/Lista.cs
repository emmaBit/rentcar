﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RentCar.Model.Models;
using RentCar.BL.Repositories;
using RentCar.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace RentCar.InspeccionForm
{
    public partial class Lista : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private IBaseRespository<Inspeccion> _db;
        RentCarContext _context;
        private ViewModel _modelSelected;
        public Lista()
        {
            _context = new RentCarContext();
            _db = new BaseRespository<Inspeccion>();
            InitializeComponent();
            LoadData();
        }

        public BindingList<ViewModel> GetDataSource()
        {
            BindingList<ViewModel> result = new BindingList<ViewModel>();


            foreach (var item in _context.Inspeccion
                .Include(x => x.Vehiculo)
                .Include(x => x.Cliente)
                .Include(x => x.Empleado)
                .ToList())
            {
                result.Add(new ViewModel
                {
                    Id = item.Id,
                    Estado = item.Estado,
                    Empleado = item.Empleado.Nombre,
                    Cliente = item.Cliente.Nombre,
                    Vehiculo = $"Placa: {item.Vehiculo.NoPlaca}",
                    CreatedDate = item.CreatedDate,
                    FechaInspeccion = item.FechaInspeccion,
                    Descripcion = item.Descripcion,
                    CantidadCombustible = item.CantidadCombustible,
                    EstadoCristalDelanteroDer = item.EstadoCristalDelanteroDer,
                    EstadoCristalDelanteroIzq = item.EstadoCristalDelanteroIzq,
                    EstadoCristalFrontal = item.EstadoCristalFrontal,
                    EstadoCristalTrasero = item.EstadoCristalTrasero,
                    EstadoCristalTraseroDer = item.EstadoCristalTraseroDer,
                    EstadoCristalTraseroIzq = item.EstadoCristalTraseroIzq,
                    EstadoGomaDelanteraDer = item.EstadoGomaDelanteraDer,
                    EstadoGomaDelanteraIzq = item.EstadoGomaDelanteraIzq,
                    EstadoGomaRepuesta = item.EstadoGomaRepuesta,
                    EstadoGomaTraseraDer = item.EstadoGomaTraseraDer,
                    EstadoGomaTraseraIzq = item.EstadoGomaTraseraIzq,
                    TieneGato= item.TieneGato,
                    TieneRalladuras = item.TieneRalladuras
                });
            }
            return result;
        }
        private void LoadData()
        {
            BindingList<ViewModel> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "Resgistros : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Visible = false;

            var nuevoInspeccion = new Crear
            {
                Visible = true
            };
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            _modelSelected = (ViewModel)row.GetFocusedRow();
        }
       
    }
    public class ViewModel
    {
        public ViewModel()
        {

        }
        public int Id { get; set; }

        public string Cliente { get; set; }
        [Display(Name = "Vehículo")]
        public string Vehiculo { get; set; }
        public string Empleado { get; set; }
        [Display(Name = "Fecha")]
        public DateTime FechaInspeccion { get; set; }
        [Display(Name = "¿Tiene Gato?")]
        public bool TieneGato { get; set; }

        [Display(Name = "¿Tiene ralladuras?")]
        public bool TieneRalladuras { get; set; }

        [Display(Name = "Combustible")]
        public string CantidadCombustible { get; set; } //1/4, 1/2, 3/4, Lleno

        [Display(Name = "Goma Frontal Izquiera")]
        public string EstadoGomaDelanteraIzq { get; set; } //Buena, Regular, Malo, Otro
        [Display(Name = "Goma Frontal Derecha")]
        public string EstadoGomaDelanteraDer { get; set; }
        [Display(Name = "Goma Trasera Izquiera")]
        public string EstadoGomaTraseraIzq { get; set; }
        [Display(Name = "Goma Trasera Derecha")]
        public string EstadoGomaTraseraDer { get; set; }
        [Display(Name = "Goma de repuesto")]
        public string EstadoGomaRepuesta { get; set; }

        [Display(Name = "Cristal Frontal Izquierdo")]
        public string EstadoCristalDelanteroIzq { get; set; }
        [Display(Name = "Cristal Frontal Derecho")]
        public string EstadoCristalDelanteroDer { get; set; }
        [Display(Name = "Cristal Trasero Izquierdo")]
        public string EstadoCristalTraseroIzq { get; set; }
        [Display(Name = "Cristal Trasero Derecho")]
        public string EstadoCristalTraseroDer { get; set; }
        [Display(Name = "Cristal Frontal")]
        public string EstadoCristalFrontal { get; set; }
        [Display(Name = "Cistal Trasero")]
        public string EstadoCristalTrasero { get; set; }
        public string Estado { get; set; }
        


        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
    }
}
