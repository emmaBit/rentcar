﻿namespace RentCar.InspeccionForm
{
    partial class Crear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.label2 = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cbbCristalTra = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbCristalFro = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbbCristalTraDer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.cbbCristalTraIzq = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.cbbCristalFroDer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.cbbCristalFroIzq = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cbbGomaRep = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.cbbGomaTraDer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbGomaTraIzq = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.cbbGomaFroDer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.cbbGomaFroIzq = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbCombustible = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.checkRalladuras = new DevExpress.XtraEditors.CheckEdit();
            this.checkGato = new DevExpress.XtraEditors.CheckEdit();
            this.cbbEmpleado = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.cbbVehiculo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbCliente = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtDescripcion = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.paneLeftBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panelBottomBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalFro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalTraDer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalTraIzq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalFroDer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalFroIzq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaRep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaTraDer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaTraIzq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaFroDer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaFroIzq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCombustible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRalladuras.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkGato.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEmpleado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbVehiculo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCliente.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(67, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cliente:";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.cbbCombustible);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.checkRalladuras);
            this.panelControl1.Controls.Add(this.checkGato);
            this.panelControl1.Controls.Add(this.cbbEmpleado);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.cbbVehiculo);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.cbbCliente);
            this.panelControl1.Controls.Add(this.txtDescripcion);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(52, 43);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(912, 619);
            this.panelControl1.TabIndex = 14;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.cbbCristalTra);
            this.groupControl2.Controls.Add(this.cbbCristalFro);
            this.groupControl2.Controls.Add(this.label16);
            this.groupControl2.Controls.Add(this.label11);
            this.groupControl2.Controls.Add(this.cbbCristalTraDer);
            this.groupControl2.Controls.Add(this.label12);
            this.groupControl2.Controls.Add(this.cbbCristalTraIzq);
            this.groupControl2.Controls.Add(this.label13);
            this.groupControl2.Controls.Add(this.cbbCristalFroDer);
            this.groupControl2.Controls.Add(this.label14);
            this.groupControl2.Controls.Add(this.cbbCristalFroIzq);
            this.groupControl2.Controls.Add(this.label15);
            this.groupControl2.Location = new System.Drawing.Point(472, 139);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(389, 319);
            this.groupControl2.TabIndex = 37;
            this.groupControl2.Text = "Estado cristales";
            // 
            // cbbCristalTra
            // 
            this.cbbCristalTra.Location = new System.Drawing.Point(163, 174);
            this.cbbCristalTra.Name = "cbbCristalTra";
            this.cbbCristalTra.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCristalTra.Properties.Appearance.Options.UseFont = true;
            this.cbbCristalTra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCristalTra.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbCristalTra.Size = new System.Drawing.Size(192, 26);
            this.cbbCristalTra.TabIndex = 39;
            // 
            // cbbCristalFro
            // 
            this.cbbCristalFro.Location = new System.Drawing.Point(163, 34);
            this.cbbCristalFro.Name = "cbbCristalFro";
            this.cbbCristalFro.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCristalFro.Properties.Appearance.Options.UseFont = true;
            this.cbbCristalFro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCristalFro.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbCristalFro.Size = new System.Drawing.Size(192, 26);
            this.cbbCristalFro.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(89, 173);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 20);
            this.label16.TabIndex = 37;
            this.label16.Text = "Trasero:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(94, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 20);
            this.label11.TabIndex = 35;
            this.label11.Text = "Frontal:";
            // 
            // cbbCristalTraDer
            // 
            this.cbbCristalTraDer.Location = new System.Drawing.Point(163, 269);
            this.cbbCristalTraDer.Name = "cbbCristalTraDer";
            this.cbbCristalTraDer.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCristalTraDer.Properties.Appearance.Options.UseFont = true;
            this.cbbCristalTraDer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCristalTraDer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbCristalTraDer.Size = new System.Drawing.Size(192, 26);
            this.cbbCristalTraDer.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(22, 272);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(141, 20);
            this.label12.TabIndex = 33;
            this.label12.Text = "Trasero Derecho:";
            // 
            // cbbCristalTraIzq
            // 
            this.cbbCristalTraIzq.Location = new System.Drawing.Point(163, 223);
            this.cbbCristalTraIzq.Name = "cbbCristalTraIzq";
            this.cbbCristalTraIzq.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCristalTraIzq.Properties.Appearance.Options.UseFont = true;
            this.cbbCristalTraIzq.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCristalTraIzq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbCristalTraIzq.Size = new System.Drawing.Size(192, 26);
            this.cbbCristalTraIzq.TabIndex = 32;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 226);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 20);
            this.label13.TabIndex = 31;
            this.label13.Text = "Trasero Izquierdo:";
            // 
            // cbbCristalFroDer
            // 
            this.cbbCristalFroDer.Location = new System.Drawing.Point(163, 124);
            this.cbbCristalFroDer.Name = "cbbCristalFroDer";
            this.cbbCristalFroDer.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCristalFroDer.Properties.Appearance.Options.UseFont = true;
            this.cbbCristalFroDer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCristalFroDer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbCristalFroDer.Size = new System.Drawing.Size(192, 26);
            this.cbbCristalFroDer.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(27, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 20);
            this.label14.TabIndex = 29;
            this.label14.Text = "Frontal Derecho:";
            // 
            // cbbCristalFroIzq
            // 
            this.cbbCristalFroIzq.Location = new System.Drawing.Point(163, 78);
            this.cbbCristalFroIzq.Name = "cbbCristalFroIzq";
            this.cbbCristalFroIzq.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCristalFroIzq.Properties.Appearance.Options.UseFont = true;
            this.cbbCristalFroIzq.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCristalFroIzq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbCristalFroIzq.Size = new System.Drawing.Size(192, 26);
            this.cbbCristalFroIzq.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(139, 20);
            this.label15.TabIndex = 27;
            this.label15.Text = "Frontal Izquierdo:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cbbGomaRep);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.cbbGomaTraDer);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.cbbGomaTraIzq);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.cbbGomaFroDer);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.cbbGomaFroIzq);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Location = new System.Drawing.Point(56, 139);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(374, 319);
            this.groupControl1.TabIndex = 25;
            this.groupControl1.Text = "Estado de gomas";
            // 
            // cbbGomaRep
            // 
            this.cbbGomaRep.Location = new System.Drawing.Point(163, 250);
            this.cbbGomaRep.Name = "cbbGomaRep";
            this.cbbGomaRep.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbGomaRep.Properties.Appearance.Options.UseFont = true;
            this.cbbGomaRep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbGomaRep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbGomaRep.Size = new System.Drawing.Size(192, 26);
            this.cbbGomaRep.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(76, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 20);
            this.label10.TabIndex = 35;
            this.label10.Text = "Repuesto:";
            // 
            // cbbGomaTraDer
            // 
            this.cbbGomaTraDer.Location = new System.Drawing.Point(163, 200);
            this.cbbGomaTraDer.Name = "cbbGomaTraDer";
            this.cbbGomaTraDer.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbGomaTraDer.Properties.Appearance.Options.UseFont = true;
            this.cbbGomaTraDer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbGomaTraDer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbGomaTraDer.Size = new System.Drawing.Size(192, 26);
            this.cbbGomaTraDer.TabIndex = 34;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(22, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 20);
            this.label9.TabIndex = 33;
            this.label9.Text = "Trasera Derecha:";
            // 
            // cbbGomaTraIzq
            // 
            this.cbbGomaTraIzq.Location = new System.Drawing.Point(163, 148);
            this.cbbGomaTraIzq.Name = "cbbGomaTraIzq";
            this.cbbGomaTraIzq.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbGomaTraIzq.Properties.Appearance.Options.UseFont = true;
            this.cbbGomaTraIzq.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbGomaTraIzq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbGomaTraIzq.Size = new System.Drawing.Size(192, 26);
            this.cbbGomaTraIzq.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 20);
            this.label8.TabIndex = 31;
            this.label8.Text = "Trasera Izquierda:";
            // 
            // cbbGomaFroDer
            // 
            this.cbbGomaFroDer.Location = new System.Drawing.Point(163, 101);
            this.cbbGomaFroDer.Name = "cbbGomaFroDer";
            this.cbbGomaFroDer.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbGomaFroDer.Properties.Appearance.Options.UseFont = true;
            this.cbbGomaFroDer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbGomaFroDer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbGomaFroDer.Size = new System.Drawing.Size(192, 26);
            this.cbbGomaFroDer.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(27, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 20);
            this.label7.TabIndex = 29;
            this.label7.Text = "Frontal Derecha:";
            // 
            // cbbGomaFroIzq
            // 
            this.cbbGomaFroIzq.Location = new System.Drawing.Point(163, 47);
            this.cbbGomaFroIzq.Name = "cbbGomaFroIzq";
            this.cbbGomaFroIzq.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbGomaFroIzq.Properties.Appearance.Options.UseFont = true;
            this.cbbGomaFroIzq.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbGomaFroIzq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbGomaFroIzq.Size = new System.Drawing.Size(192, 26);
            this.cbbGomaFroIzq.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 20);
            this.label6.TabIndex = 27;
            this.label6.Text = "Frontal Izquierda:";
            // 
            // cbbCombustible
            // 
            this.cbbCombustible.Location = new System.Drawing.Point(626, 79);
            this.cbbCombustible.Name = "cbbCombustible";
            this.cbbCombustible.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCombustible.Properties.Appearance.Options.UseFont = true;
            this.cbbCombustible.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCombustible.Size = new System.Drawing.Size(235, 32);
            this.cbbCombustible.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(493, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 25);
            this.label5.TabIndex = 23;
            this.label5.Text = "Combustible:";
            // 
            // checkRalladuras
            // 
            this.checkRalladuras.Location = new System.Drawing.Point(49, 543);
            this.checkRalladuras.Name = "checkRalladuras";
            this.checkRalladuras.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkRalladuras.Properties.Appearance.Options.UseFont = true;
            this.checkRalladuras.Properties.Caption = "Tiene Ralladuras";
            this.checkRalladuras.Size = new System.Drawing.Size(194, 28);
            this.checkRalladuras.TabIndex = 19;
            // 
            // checkGato
            // 
            this.checkGato.Location = new System.Drawing.Point(49, 487);
            this.checkGato.Name = "checkGato";
            this.checkGato.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkGato.Properties.Appearance.Options.UseFont = true;
            this.checkGato.Properties.Caption = "Tiene Gato";
            this.checkGato.Size = new System.Drawing.Size(132, 28);
            this.checkGato.TabIndex = 18;
            // 
            // cbbEmpleado
            // 
            this.cbbEmpleado.Location = new System.Drawing.Point(152, 79);
            this.cbbEmpleado.Name = "cbbEmpleado";
            this.cbbEmpleado.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbEmpleado.Properties.Appearance.Options.UseFont = true;
            this.cbbEmpleado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbEmpleado.Size = new System.Drawing.Size(291, 32);
            this.cbbEmpleado.TabIndex = 15;
            this.cbbEmpleado.SelectedIndexChanged += new System.EventHandler(this.cbbEmpleado_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 25);
            this.label4.TabIndex = 14;
            this.label4.Text = "Empleado:";
            // 
            // cbbVehiculo
            // 
            this.cbbVehiculo.Location = new System.Drawing.Point(626, 26);
            this.cbbVehiculo.Name = "cbbVehiculo";
            this.cbbVehiculo.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbVehiculo.Properties.Appearance.Options.UseFont = true;
            this.cbbVehiculo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbVehiculo.Size = new System.Drawing.Size(235, 32);
            this.cbbVehiculo.TabIndex = 13;
            this.cbbVehiculo.SelectedIndexChanged += new System.EventHandler(this.cbbVehiculo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(460, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Vehículo (placa):";
            // 
            // cbbCliente
            // 
            this.cbbCliente.Location = new System.Drawing.Point(152, 26);
            this.cbbCliente.Name = "cbbCliente";
            this.cbbCliente.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCliente.Properties.Appearance.Options.UseFont = true;
            this.cbbCliente.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCliente.Size = new System.Drawing.Size(291, 32);
            this.cbbCliente.TabIndex = 11;
            this.cbbCliente.SelectedIndexChanged += new System.EventHandler(this.cbbCliente_SelectedIndexChanged);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(465, 505);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(362, 66);
            this.txtDescripcion.TabIndex = 7;
            this.txtDescripcion.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(461, 471);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Descripción:";
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.Location = new System.Drawing.Point(52, 0);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(12, 6, 0, 0);
            this.labelControl1.Size = new System.Drawing.Size(912, 43);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Proceso de inspección";
            // 
            // paneLeftBtn
            // 
            this.paneLeftBtn.BackColor = System.Drawing.Color.White;
            this.paneLeftBtn.ButtonInterval = 0;
            windowsUIButtonImageOptions1.ImageUri.Uri = "Backward;Size32x32;GrayScaled";
            this.paneLeftBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Regresar", -1, false)});
            this.paneLeftBtn.ContentAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.paneLeftBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.paneLeftBtn.ForeColor = System.Drawing.Color.Gray;
            this.paneLeftBtn.Location = new System.Drawing.Point(0, 0);
            this.paneLeftBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.paneLeftBtn.MaximumSize = new System.Drawing.Size(52, 0);
            this.paneLeftBtn.MinimumSize = new System.Drawing.Size(52, 0);
            this.paneLeftBtn.Name = "paneLeftBtn";
            this.paneLeftBtn.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.paneLeftBtn.Padding = new System.Windows.Forms.Padding(6, 6, 0, 0);
            this.paneLeftBtn.Size = new System.Drawing.Size(52, 662);
            this.paneLeftBtn.TabIndex = 11;
            this.paneLeftBtn.Text = "windowsUIButtonPanel1";
            this.paneLeftBtn.UseButtonBackgroundImages = false;
            this.paneLeftBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelLeftBtnClick);
            // 
            // panelBottomBtn
            // 
            this.panelBottomBtn.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.panelBottomBtn.AppearanceButton.Normal.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Normal.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.panelBottomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            windowsUIButtonImageOptions2.ImageUri.Uri = "Save;Size32x32;GrayScaled";
            windowsUIButtonImageOptions3.ImageUri.Uri = "Reset;Size32x32;GrayScaled";
            this.panelBottomBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Guardar", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Guardar", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Limpiar", -1, false)});
            this.panelBottomBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottomBtn.EnableImageTransparency = true;
            this.panelBottomBtn.ForeColor = System.Drawing.Color.White;
            this.panelBottomBtn.Location = new System.Drawing.Point(0, 662);
            this.panelBottomBtn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panelBottomBtn.MaximumSize = new System.Drawing.Size(0, 74);
            this.panelBottomBtn.MinimumSize = new System.Drawing.Size(70, 74);
            this.panelBottomBtn.Name = "panelBottomBtn";
            this.panelBottomBtn.Size = new System.Drawing.Size(964, 74);
            this.panelBottomBtn.TabIndex = 12;
            this.panelBottomBtn.Text = "windowsUIButtonPanelMain";
            this.panelBottomBtn.UseButtonBackgroundImages = false;
            this.panelBottomBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelBottomBtnClick);
            // 
            // Crear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 736);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.paneLeftBtn);
            this.Controls.Add(this.panelBottomBtn);
            this.Name = "Crear";
            this.Text = "Crear";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalFro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalTraDer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalTraIzq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalFroDer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCristalFroIzq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaRep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaTraDer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaTraIzq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaFroDer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGomaFroIzq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCombustible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRalladuras.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkGato.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEmpleado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbVehiculo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCliente.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.RichTextBox txtDescripcion;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel paneLeftBtn;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel panelBottomBtn;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbbEmpleado;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbbVehiculo;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbbCliente;
        private DevExpress.XtraEditors.CheckEdit checkGato;
        private DevExpress.XtraEditors.CheckEdit checkRalladuras;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCombustible;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCristalTra;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCristalFro;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCristalTraDer;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCristalTraIzq;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCristalFroDer;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.ComboBoxEdit cbbCristalFroIzq;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.ComboBoxEdit cbbGomaRep;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.ComboBoxEdit cbbGomaTraDer;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.ComboBoxEdit cbbGomaTraIzq;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.ComboBoxEdit cbbGomaFroDer;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.ComboBoxEdit cbbGomaFroIzq;
        private System.Windows.Forms.Label label6;
    }
}