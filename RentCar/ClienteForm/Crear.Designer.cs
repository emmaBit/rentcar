﻿namespace RentCar.ClienteForm
{
    partial class Crear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.paneLeftBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panelBottomBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbTipos = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtLimiteCR = new System.Windows.Forms.NumericUpDown();
            this.txtCedula = new System.Windows.Forms.MaskedTextBox();
            this.txtNoTarjetaCR = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTipos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLimiteCR)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtCedula);
            this.panelControl1.Controls.Add(this.txtLimiteCR);
            this.panelControl1.Controls.Add(this.cbbTipos);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.txtNoTarjetaCR);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.txtNombre);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(46, 43);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1308, 303);
            this.panelControl1.TabIndex = 14;
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(316, 21);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(875, 40);
            this.txtNombre.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(187, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 34);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre:";
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.Location = new System.Drawing.Point(46, 0);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(10, 6, 0, 0);
            this.labelControl1.Size = new System.Drawing.Size(1308, 43);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Cliente";
            // 
            // paneLeftBtn
            // 
            this.paneLeftBtn.BackColor = System.Drawing.Color.White;
            this.paneLeftBtn.ButtonInterval = 0;
            windowsUIButtonImageOptions1.ImageUri.Uri = "Backward;Size32x32;GrayScaled";
            this.paneLeftBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Regresar", -1, false)});
            this.paneLeftBtn.ContentAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.paneLeftBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.paneLeftBtn.ForeColor = System.Drawing.Color.Gray;
            this.paneLeftBtn.Location = new System.Drawing.Point(0, 0);
            this.paneLeftBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.paneLeftBtn.MaximumSize = new System.Drawing.Size(46, 0);
            this.paneLeftBtn.MinimumSize = new System.Drawing.Size(46, 0);
            this.paneLeftBtn.Name = "paneLeftBtn";
            this.paneLeftBtn.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.paneLeftBtn.Padding = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.paneLeftBtn.Size = new System.Drawing.Size(46, 346);
            this.paneLeftBtn.TabIndex = 11;
            this.paneLeftBtn.Text = "windowsUIButtonPanel1";
            this.paneLeftBtn.UseButtonBackgroundImages = false;
            this.paneLeftBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelLeftBtnClick);
            // 
            // panelBottomBtn
            // 
            this.panelBottomBtn.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.panelBottomBtn.AppearanceButton.Normal.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Normal.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.panelBottomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            windowsUIButtonImageOptions2.ImageUri.Uri = "Save;Size32x32;GrayScaled";
            windowsUIButtonImageOptions3.ImageUri.Uri = "Reset;Size32x32;GrayScaled";
            this.panelBottomBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Guardar", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Guardar", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Limpiar", -1, false)});
            this.panelBottomBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottomBtn.EnableImageTransparency = true;
            this.panelBottomBtn.ForeColor = System.Drawing.Color.White;
            this.panelBottomBtn.Location = new System.Drawing.Point(0, 346);
            this.panelBottomBtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.panelBottomBtn.MaximumSize = new System.Drawing.Size(0, 74);
            this.panelBottomBtn.MinimumSize = new System.Drawing.Size(61, 74);
            this.panelBottomBtn.Name = "panelBottomBtn";
            this.panelBottomBtn.Size = new System.Drawing.Size(1354, 74);
            this.panelBottomBtn.TabIndex = 12;
            this.panelBottomBtn.Text = "windowsUIButtonPanelMain";
            this.panelBottomBtn.UseButtonBackgroundImages = false;
            this.panelBottomBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelBottomBtnClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(201, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 34);
            this.label1.TabIndex = 7;
            this.label1.Text = "Cédula:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(810, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 34);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tipo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(657, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(230, 34);
            this.label4.TabIndex = 13;
            this.label4.Text = "Límite de crédito:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(295, 34);
            this.label5.TabIndex = 11;
            this.label5.Text = "No. Tarjeta de crédito:";
            // 
            // cbbTipos
            // 
            this.cbbTipos.Location = new System.Drawing.Point(909, 99);
            this.cbbTipos.Name = "cbbTipos";
            this.cbbTipos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTipos.Properties.Appearance.Options.UseFont = true;
            this.cbbTipos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbTipos.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbTipos.Size = new System.Drawing.Size(282, 40);
            this.cbbTipos.TabIndex = 15;
            // 
            // txtLimiteCR
            // 
            this.txtLimiteCR.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLimiteCR.Location = new System.Drawing.Point(909, 189);
            this.txtLimiteCR.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtLimiteCR.Name = "txtLimiteCR";
            this.txtLimiteCR.Size = new System.Drawing.Size(282, 40);
            this.txtLimiteCR.TabIndex = 16;
            // 
            // txtCedula
            // 
            this.txtCedula.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCedula.Location = new System.Drawing.Point(316, 108);
            this.txtCedula.Mask = "000-0000000-0";
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(319, 35);
            this.txtCedula.TabIndex = 17;
            // 
            // txtNoTarjetaCR
            // 
            this.txtNoTarjetaCR.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoTarjetaCR.Location = new System.Drawing.Point(316, 188);
            this.txtNoTarjetaCR.Name = "txtNoTarjetaCR";
            this.txtNoTarjetaCR.Size = new System.Drawing.Size(319, 40);
            this.txtNoTarjetaCR.TabIndex = 12;
            // 
            // Crear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 420);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.paneLeftBtn);
            this.Controls.Add(this.panelBottomBtn);
            this.Name = "Crear";
            this.Text = "Crear";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTipos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLimiteCR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel paneLeftBtn;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel panelBottomBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ComboBoxEdit cbbTipos;
        private System.Windows.Forms.NumericUpDown txtLimiteCR;
        private System.Windows.Forms.MaskedTextBox txtCedula;
        private System.Windows.Forms.TextBox txtNoTarjetaCR;
    }
}