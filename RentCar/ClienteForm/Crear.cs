﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using RentCar.Model.Extensions;
using System.Windows.Forms;

namespace RentCar.ClienteForm
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Cliente model;
        private Cliente _editingEntity;
        private BaseRespository<Cliente> _db;
        public Crear()
        {
            model = new Cliente();
            Init();

        }
        void Init()
        {
            _db = new BaseRespository<Cliente>();
            InitializeComponent();
            cbbTipos.Properties.Items.Add("Física");
            cbbTipos.Properties.Items.Add("Jurídica");
        }
        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }

        public Crear(Cliente editingEntity)
        {
            _editingEntity = editingEntity;
          
            Init();
            txtNombre.Text = _editingEntity.Nombre;
            txtCedula.Text = _editingEntity.Cedula;
            txtNoTarjetaCR.Text = _editingEntity.NoTarjetaCR;
            txtLimiteCR.Value = _editingEntity.LimiteCredito;
            cbbTipos.EditValue = _editingEntity.TipoPersona;
        }

        void Save()
        {
            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var cedula = "";
            if (txtCedula.Text != null && txtCedula.Text.Length == 13)
            {
                cedula = txtCedula.Text.ToNumberCedula();
            }
            if (_editingEntity != null)
            {
                _editingEntity.Nombre = txtNombre.Text;
                _editingEntity.Cedula = cedula;
                _editingEntity.NoTarjetaCR = txtNoTarjetaCR.Text;
                _editingEntity.LimiteCredito = txtLimiteCR.Value;
                _editingEntity.TipoPersona = cbbTipos.Text;
                _db.Update(_editingEntity);
            }
            else
            {
                model.Nombre = txtNombre.Text;
                model.Cedula = cedula;
                model.NoTarjetaCR = txtNoTarjetaCR.Text;
                model.LimiteCredito = txtLimiteCR.Value;
                model.TipoPersona = cbbTipos.EditValue.ToString();

                model.Estado = "Activo";
                _db.Add(model);
            }
            _db.SaveChanges();
            Limpiar();
            Regresar();

        }
        void Limpiar()
        {
            model = new Cliente();
            txtNombre.Text = model.Nombre;
            txtCedula.Text = model.Cedula;
            txtNoTarjetaCR.Text = model.NoTarjetaCR;
            txtLimiteCR.Value = 0;
            txtLimiteCR.ResetText();
            cbbTipos.EditValue = null;
        }
        void Regresar()
        {
            Visible = false;
            new Lista
            {
                Visible = true
            };
        }

        bool ModelIsValid()
        {
            if (txtCedula.Text == null || txtCedula.Text == "" ||
              txtLimiteCR.Text == null || txtLimiteCR.Text == "" ||
              txtNombre.Text == null || txtNombre.Text == "" ||
              txtNoTarjetaCR.Text == null || txtNoTarjetaCR.Text == "" ||
              cbbTipos.Text == null || cbbTipos.Text == "")
                return false;

            return true;
        }
    }
}
