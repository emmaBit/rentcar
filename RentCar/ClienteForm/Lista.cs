﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RentCar.Model.Models;
using RentCar.BL.Repositories;
using RentCar.Model.Extensions;

namespace RentCar.ClienteForm
{
    public partial class Lista : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private IBaseRespository<Cliente> _db;
        private ViewModel _modelSelected;
        public Lista()
        {
            _db = new BaseRespository<Cliente>();
            InitializeComponent();
            LoadData();
        }

        public BindingList<ViewModel> GetDataSource()
        {
            BindingList<ViewModel> result = new BindingList<ViewModel>();

            foreach (var item in _db.GetAll())
            {
                var cedula = "";
                if(item.Cedula != null && item.Cedula.Length == 11)
                {
                    cedula = item.Cedula.ToLocalCedula();
                }
                result.Add(new ViewModel
                {
                    Id = item.Id,
                    Nombre = item.Nombre,
                    Cedula = cedula,
                    Estado = item.Estado,
                    NoTarjetaCr = item.NoTarjetaCR,
                    LimiteCredito = item.LimiteCredito,
                    TipoPersona = item.TipoPersona,
                    CreatedDate = item.CreatedDate
                });
            }
            return result;
        }
        private void LoadData()
        {
            BindingList<ViewModel> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "Resgistros : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Visible = false;

            var nuevoCliente = new Crear
            {
                Visible = true
            };
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            _modelSelected = (ViewModel)row.GetFocusedRow();
        }

        private void btnEditar_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                Visible = false;
                var _editingModel = new Cliente
                {
                    Id = _modelSelected.Id,
                    Nombre = _modelSelected.Nombre,
                    Cedula = _modelSelected.Cedula,
                    Estado = _modelSelected.Estado,
                    NoTarjetaCR = _modelSelected.NoTarjetaCr,
                    LimiteCredito = _modelSelected.LimiteCredito,
                    TipoPersona = _modelSelected.TipoPersona,
                    CreatedDate = _modelSelected.CreatedDate
                };
                new Crear(_editingModel).Visible = true;
            }

        }

        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                var result = MessageBox.Show("¿Borrar este registro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    _db.Remove(_modelSelected.Id);
                    _db.SaveChanges();
                    LoadData();
                }
            }
        }
    }

    public class ViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [Display(Name = "Cédula")]
        public string Cedula { get; set; }
        [Display(Name = "Tipo")]
        public string TipoPersona { get; set; }
        [Display(Name = "No. Tarjeta de crédito")]
        public string NoTarjetaCr { get; set; }
        [Display(Name = "Límite de créedito")]
        public decimal LimiteCredito { get; set; }
        public string Estado { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
    }
}
