﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using System;
using System.Windows.Forms;

namespace RentCar.ModeloForm
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Modelo model;
        private Modelo _editingEntity;
        private BaseRespository<Modelo> _db;
        private BaseRespository<Marca> _dbMarcas;
        public Crear()
        {
            model = new Modelo();
            Init();
        }
        private void Init() {
            _db = new BaseRespository<Modelo>();
            _dbMarcas = new BaseRespository<Marca>();
            InitializeComponent();
            LoadMarcas();
        }
        private void LoadMarcas()
        {
            var items = cbbMarcas.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (var marca in _dbMarcas.GetAll())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(marca.Nombre, value: marca.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
           
        }
        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }


        public Crear(Modelo editingEntity)
        {
            _editingEntity = editingEntity;
            Init();
            txtNombre.Text = _editingEntity.Nombre;
            txtDescripcion.Text = _editingEntity.Descripcion;
            cbbMarcas.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(editingEntity.Marca.Nombre, value: editingEntity.Marca.Id);
        }

        void Save()
        {
            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (_editingEntity != null)
            {
                _editingEntity.Nombre = txtNombre.Text;
                _editingEntity.Descripcion = txtDescripcion.Text;
                _editingEntity.Marca = null;
                _db.Update(_editingEntity);
            }
            else
            {
                model.Nombre = txtNombre.Text;
                model.Descripcion = txtDescripcion.Text;

                model.Estado = "Activo";
                _db.Add(model);
            }
            _db.SaveChanges();
            Limpiar();
            Regresar();

        }
        void Limpiar()
        {
            model = new Modelo();
            txtNombre.Text = model.Nombre;
            txtDescripcion.Text = model.Descripcion;
            cbbMarcas.EditValue = null;
        }
        void Regresar()
        {
            Visible = false;
            new Lista
            {
                Visible = true
            };
        }

        private void imageComboBoxEdit1_SelectedValueChanged(object sender, System.EventArgs e)
        {
            if(sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if(selected != null)
                {
                    if (_editingEntity != null)
                    {
                        _editingEntity.MarcaId = Int32.Parse(selected?.ToString());
                    }
                    else
                    {
                        model.MarcaId = Int32.Parse(selected?.ToString());
                    }
                }
                
            }

        }

        bool ModelIsValid()
        {
            if (txtNombre.Text == null || txtNombre.Text == "" ||
                cbbMarcas.Text == null || cbbMarcas.Text == ""
                )
                return false;

            return true;
        }
    }
}
