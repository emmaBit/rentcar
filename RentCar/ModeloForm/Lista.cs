﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RentCar.Model.Models;
using RentCar.BL.Repositories;
using RentCar.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace RentCar.ModeloForm
{
    public partial class Lista : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private IBaseRespository<Modelo> _db;
        RentCarContext _context;
        private ViewModel _modelSelected;
        public Lista()
        {
            _context = new RentCarContext();
            _db = new BaseRespository<Modelo>();
            InitializeComponent();
            LoadData();
        }

        public BindingList<ViewModel> GetDataSource()
        {
            BindingList<ViewModel> result = new BindingList<ViewModel>();

            
            foreach (var item in _context.Modelo.Include(x=> x.Marca).ToList())
            {
                result.Add(new ViewModel
                {
                    Id = item.Id,
                    Estado = item.Estado,
                    Nombre = item.Nombre,
                    Marca = item.Marca?.Nombre,
                    Descripcion = item.Descripcion,
                    CreatedDate = item.CreatedDate,
                });
            }
            return result;
        }
        private void LoadData()
        {
            BindingList<ViewModel> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "Resgistros : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Visible = false;

            var nuevoModelo = new Crear
            {
                Visible = true
            };
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            _modelSelected = (ViewModel)row.GetFocusedRow();
        }

        private void btnEditar_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                var obj = _context.Modelo.Include(x => x.Marca).FirstOrDefault(x => x.Id == _modelSelected.Id);
                Visible = false;
                new Crear(obj).Visible = true;
            }

        }

        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                var result = MessageBox.Show("¿Borrar este registro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    _db.Remove(_modelSelected.Id);
                    _db.SaveChanges();
                    LoadData();
                }
            }
        }
    }

    public class ViewModel
    {
        public ViewModel()
        {

        }

        public ViewModel(int id, string nombre)
        {
            Id = id;
            Nombre = nombre;
        }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Marca { get; set; }
        public string Estado { get; set; }
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
    }
}
