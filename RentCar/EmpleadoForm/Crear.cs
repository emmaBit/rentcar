﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using RentCar.Model.Extensions;
using System.Windows.Forms;

namespace RentCar.EmpleadoForm
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Empleado model;
        private Empleado _editingEntity;
        private BaseRespository<Empleado> _db;
        public Crear()
        {
            model = new Empleado();
            Init();

        }
        void Init()
        {
            _db = new BaseRespository<Empleado>();
            InitializeComponent();
            cbbTandaLabor.Properties.Items.Add("Matutina");
            cbbTandaLabor.Properties.Items.Add("Vespertina");
            cbbTandaLabor.Properties.Items.Add("Nocturna");
        }
        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }

        public Crear(Empleado editingEntity)
        {
            _editingEntity = editingEntity;

            Init();
            txtNombre.Text = _editingEntity.Nombre;
            txtCedula.Text = _editingEntity.Cedula;
            txtComision.Value = _editingEntity.PorcientoComision;
            txtFechaIngreso.DateTime = _editingEntity.FechaIngreso;
            cbbTandaLabor.EditValue = _editingEntity.TandaLabor;
        }

        void Save()
        {

            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            var cedula = "";
            if (txtCedula.Text != null && txtCedula.Text.Length == 13)
            {
                cedula = txtCedula.Text.ToNumberCedula();
            }
            if (_editingEntity != null)
            {
                _editingEntity.Nombre = txtNombre.Text;
                _editingEntity.Cedula = cedula;
                _editingEntity.PorcientoComision = (int)txtComision.Value;
                _editingEntity.FechaIngreso = txtFechaIngreso.DateTime;
                _editingEntity.TandaLabor = cbbTandaLabor.EditValue.ToString();
                _db.Update(_editingEntity);
            }
            else
            {
                model.Nombre = txtNombre.Text;
                model.Cedula = cedula;
                model.PorcientoComision = (int)txtComision.Value;
                model.FechaIngreso = txtFechaIngreso.DateTime;
                model.TandaLabor = cbbTandaLabor.EditValue.ToString();

                model.Estado = "Activo";
                _db.Add(model);
            }
            _db.SaveChanges();
            Limpiar();
            Regresar();
        }
        void Limpiar()
        {
            model = new Empleado();
            txtNombre.Text = model.Nombre;
            txtCedula.Text = model.Cedula;
            txtFechaIngreso.ResetText();
            txtComision.Value = 0;
            txtComision.ResetText();
            cbbTandaLabor.EditValue = null;
        }
        void Regresar()
        {
            Visible = false;
            new Lista
            {
                Visible = true
            };
        }

        bool ModelIsValid()
        {
            if (txtCedula.Text == null || txtCedula.Text == "" ||
              txtComision.Text == null || txtComision.Text == "" ||
              txtNombre.Text == null || txtNombre.Text == "" ||
              cbbTandaLabor.Text == null || cbbTandaLabor.Text == "" ||
              txtFechaIngreso.Text == null || txtFechaIngreso.Text == "")
                return false;

            return true;
        }
    }
}
