﻿using RentCar.BL.Services;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar
{
    public class Register
    {

        public Container RegisterAllInstances()
        {
            // 1. Create a new Simple Injector container
           var _container = new Container();
            _container.Register<IVehiculoService, VehiculoService>();
            // 3. Verify your configuration
            _container.Verify();

            return _container;
        }
    }
}
