﻿namespace RentCar.Home
{
    partial class Crear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.lblTitleRenta = new DevExpress.XtraEditors.LabelControl();
            this.paneLeftBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panelBottomBtn = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.separatorControl2 = new DevExpress.XtraEditors.SeparatorControl();
            this.txtFechaDevolucion = new DevExpress.XtraEditors.DateEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCantidadDias = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMontoDia = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFechaRenta = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbEmpleado = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.cbbCliente = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtComentario = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPlacaVehiculo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaDevolucion.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaDevolucion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCantidadDias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaRenta.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaRenta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEmpleado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCliente.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitleRenta
            // 
            this.lblTitleRenta.AllowHtmlString = true;
            this.lblTitleRenta.Appearance.BackColor = System.Drawing.Color.White;
            this.lblTitleRenta.Appearance.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitleRenta.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.lblTitleRenta.Appearance.Options.UseBackColor = true;
            this.lblTitleRenta.Appearance.Options.UseFont = true;
            this.lblTitleRenta.Appearance.Options.UseForeColor = true;
            this.lblTitleRenta.Appearance.Options.UseTextOptions = true;
            this.lblTitleRenta.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTitleRenta.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTitleRenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleRenta.Location = new System.Drawing.Point(46, 0);
            this.lblTitleRenta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTitleRenta.Name = "lblTitleRenta";
            this.lblTitleRenta.Padding = new System.Windows.Forms.Padding(10, 6, 0, 0);
            this.lblTitleRenta.Size = new System.Drawing.Size(1028, 43);
            this.lblTitleRenta.TabIndex = 13;
            this.lblTitleRenta.Text = "Renta del vehículo con número de placa:";
            // 
            // paneLeftBtn
            // 
            this.paneLeftBtn.BackColor = System.Drawing.Color.White;
            this.paneLeftBtn.ButtonInterval = 0;
            windowsUIButtonImageOptions4.ImageUri.Uri = "Backward;Size32x32;GrayScaled";
            this.paneLeftBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Regresar", -1, false)});
            this.paneLeftBtn.ContentAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.paneLeftBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.paneLeftBtn.ForeColor = System.Drawing.Color.Gray;
            this.paneLeftBtn.Location = new System.Drawing.Point(0, 0);
            this.paneLeftBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.paneLeftBtn.MaximumSize = new System.Drawing.Size(46, 0);
            this.paneLeftBtn.MinimumSize = new System.Drawing.Size(46, 0);
            this.paneLeftBtn.Name = "paneLeftBtn";
            this.paneLeftBtn.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.paneLeftBtn.Padding = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.paneLeftBtn.Size = new System.Drawing.Size(46, 430);
            this.paneLeftBtn.TabIndex = 11;
            this.paneLeftBtn.Text = "windowsUIButtonPanel1";
            this.paneLeftBtn.UseButtonBackgroundImages = false;
            this.paneLeftBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelLeftBtnClick);
            // 
            // panelBottomBtn
            // 
            this.panelBottomBtn.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.panelBottomBtn.AppearanceButton.Normal.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Normal.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.panelBottomBtn.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseFont = true;
            this.panelBottomBtn.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.panelBottomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            windowsUIButtonImageOptions5.ImageUri.Uri = "Apply;Size32x32;GrayScaled";
            windowsUIButtonImageOptions6.ImageUri.Uri = "Reset;Size32x32;GrayScaled";
            this.panelBottomBtn.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Guardar", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Guardar", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Limpiar", -1, false)});
            this.panelBottomBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottomBtn.EnableImageTransparency = true;
            this.panelBottomBtn.ForeColor = System.Drawing.Color.White;
            this.panelBottomBtn.Location = new System.Drawing.Point(0, 430);
            this.panelBottomBtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.panelBottomBtn.MaximumSize = new System.Drawing.Size(0, 74);
            this.panelBottomBtn.MinimumSize = new System.Drawing.Size(61, 74);
            this.panelBottomBtn.Name = "panelBottomBtn";
            this.panelBottomBtn.Size = new System.Drawing.Size(1074, 74);
            this.panelBottomBtn.TabIndex = 12;
            this.panelBottomBtn.Text = "windowsUIButtonPanelMain";
            this.panelBottomBtn.UseButtonBackgroundImages = false;
            this.panelBottomBtn.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.onPanelBottomBtnClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(46, 43);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 387);
            this.panelControl1.TabIndex = 14;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.separatorControl2);
            this.panelControl2.Controls.Add(this.txtFechaDevolucion);
            this.panelControl2.Controls.Add(this.label11);
            this.panelControl2.Controls.Add(this.separatorControl1);
            this.panelControl2.Controls.Add(this.label9);
            this.panelControl2.Controls.Add(this.lblTotal);
            this.panelControl2.Controls.Add(this.label3);
            this.panelControl2.Controls.Add(this.txtCantidadDias);
            this.panelControl2.Controls.Add(this.label7);
            this.panelControl2.Controls.Add(this.txtMontoDia);
            this.panelControl2.Controls.Add(this.label6);
            this.panelControl2.Controls.Add(this.txtFechaRenta);
            this.panelControl2.Controls.Add(this.label5);
            this.panelControl2.Controls.Add(this.cbbEmpleado);
            this.panelControl2.Controls.Add(this.label4);
            this.panelControl2.Controls.Add(this.cbbCliente);
            this.panelControl2.Controls.Add(this.txtComentario);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1024, 383);
            this.panelControl2.TabIndex = 15;
            // 
            // separatorControl2
            // 
            this.separatorControl2.Location = new System.Drawing.Point(575, 167);
            this.separatorControl2.LookAndFeel.SkinName = "Seven Classic";
            this.separatorControl2.Name = "separatorControl2";
            this.separatorControl2.Size = new System.Drawing.Size(340, 24);
            this.separatorControl2.TabIndex = 49;
            // 
            // txtFechaDevolucion
            // 
            this.txtFechaDevolucion.EditValue = null;
            this.txtFechaDevolucion.Location = new System.Drawing.Point(219, 124);
            this.txtFechaDevolucion.Name = "txtFechaDevolucion";
            this.txtFechaDevolucion.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaDevolucion.Properties.Appearance.Options.UseFont = true;
            this.txtFechaDevolucion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFechaDevolucion.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFechaDevolucion.Size = new System.Drawing.Size(291, 32);
            this.txtFechaDevolucion.TabIndex = 48;
            this.txtFechaDevolucion.EditValueChanged += new System.EventHandler(this.txtFechaDevolucion_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 127);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(199, 25);
            this.label11.TabIndex = 47;
            this.label11.Text = "Fecha de devolución:";
            // 
            // separatorControl1
            // 
            this.separatorControl1.Location = new System.Drawing.Point(0, 222);
            this.separatorControl1.Name = "separatorControl1";
            this.separatorControl1.Size = new System.Drawing.Size(1014, 24);
            this.separatorControl1.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(893, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 25);
            this.label9.TabIndex = 45;
            this.label9.Text = "x";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(758, 194);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(61, 25);
            this.lblTotal.TabIndex = 44;
            this.lblTotal.Text = "RD$0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(690, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 25);
            this.label3.TabIndex = 43;
            this.label3.Text = "Total:";
            // 
            // txtCantidadDias
            // 
            this.txtCantidadDias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidadDias.Location = new System.Drawing.Point(767, 126);
            this.txtCantidadDias.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtCantidadDias.Name = "txtCantidadDias";
            this.txtCantidadDias.Size = new System.Drawing.Size(120, 30);
            this.txtCantidadDias.TabIndex = 42;
            this.txtCantidadDias.ValueChanged += new System.EventHandler(this.txtCantidadDias_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(587, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 25);
            this.label7.TabIndex = 41;
            this.label7.Text = "Cantidad de dias:";
            // 
            // txtMontoDia
            // 
            this.txtMontoDia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMontoDia.Location = new System.Drawing.Point(767, 78);
            this.txtMontoDia.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtMontoDia.Name = "txtMontoDia";
            this.txtMontoDia.Size = new System.Drawing.Size(120, 30);
            this.txtMontoDia.TabIndex = 40;
            this.txtMontoDia.ValueChanged += new System.EventHandler(this.txtMontoDia_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(630, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 25);
            this.label6.TabIndex = 39;
            this.label6.Text = "Monto x Dia:";
            // 
            // txtFechaRenta
            // 
            this.txtFechaRenta.EditValue = null;
            this.txtFechaRenta.Location = new System.Drawing.Point(219, 76);
            this.txtFechaRenta.Name = "txtFechaRenta";
            this.txtFechaRenta.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaRenta.Properties.Appearance.Options.UseFont = true;
            this.txtFechaRenta.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFechaRenta.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFechaRenta.Size = new System.Drawing.Size(291, 32);
            this.txtFechaRenta.TabIndex = 38;
            this.txtFechaRenta.EditValueChanged += new System.EventHandler(this.txtFechaRenta_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(55, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 25);
            this.label5.TabIndex = 23;
            this.label5.Text = "Fecha de renta:";
            // 
            // cbbEmpleado
            // 
            this.cbbEmpleado.Location = new System.Drawing.Point(682, 26);
            this.cbbEmpleado.Name = "cbbEmpleado";
            this.cbbEmpleado.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbEmpleado.Properties.Appearance.Options.UseFont = true;
            this.cbbEmpleado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbEmpleado.Size = new System.Drawing.Size(291, 32);
            this.cbbEmpleado.TabIndex = 15;
            this.cbbEmpleado.SelectedIndexChanged += new System.EventHandler(this.cbbEmpleado_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(565, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 25);
            this.label4.TabIndex = 14;
            this.label4.Text = "Empleado:";
            // 
            // cbbCliente
            // 
            this.cbbCliente.Location = new System.Drawing.Point(219, 26);
            this.cbbCliente.Name = "cbbCliente";
            this.cbbCliente.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCliente.Properties.Appearance.Options.UseFont = true;
            this.cbbCliente.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCliente.Size = new System.Drawing.Size(291, 32);
            this.cbbCliente.TabIndex = 11;
            this.cbbCliente.SelectedIndexChanged += new System.EventHandler(this.cbbCliente_SelectedIndexChanged);
            // 
            // txtComentario
            // 
            this.txtComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentario.Location = new System.Drawing.Point(195, 267);
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(778, 84);
            this.txtComentario.TabIndex = 7;
            this.txtComentario.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(123, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cliente:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 267);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Comentario:";
            // 
            // lblPlacaVehiculo
            // 
            this.lblPlacaVehiculo.AutoSize = true;
            this.lblPlacaVehiculo.BackColor = System.Drawing.Color.White;
            this.lblPlacaVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlacaVehiculo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.lblPlacaVehiculo.Location = new System.Drawing.Point(535, 11);
            this.lblPlacaVehiculo.Name = "lblPlacaVehiculo";
            this.lblPlacaVehiculo.Size = new System.Drawing.Size(39, 32);
            this.lblPlacaVehiculo.TabIndex = 47;
            this.lblPlacaVehiculo.Text = "...";
            // 
            // Crear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 504);
            this.Controls.Add(this.lblPlacaVehiculo);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.lblTitleRenta);
            this.Controls.Add(this.paneLeftBtn);
            this.Controls.Add(this.panelBottomBtn);
            this.Name = "Crear";
            this.Text = "Crear";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaDevolucion.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaDevolucion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCantidadDias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaRenta.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaRenta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEmpleado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCliente.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl lblTitleRenta;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel paneLeftBtn;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel panelBottomBtn;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SeparatorControl separatorControl2;
        private DevExpress.XtraEditors.DateEdit txtFechaDevolucion;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtCantidadDias;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown txtMontoDia;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.DateEdit txtFechaRenta;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbbEmpleado;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbbCliente;
        private System.Windows.Forms.RichTextBox txtComentario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPlacaVehiculo;
    }
}