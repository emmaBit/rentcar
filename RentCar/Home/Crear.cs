﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using RentCar.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using RentCar.Model.Generic;
using SimpleInjector;
using RentCar.BL.Services;
using System.Windows.Forms;
using RentCar.BL;

namespace RentCar.Home
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Renta model;
        private Inspeccion _inspeccion;
        private Renta _devolucionEntity;
        private BaseRespository<Renta> _db;
        private IRentCarContext _context;
        Container _container ;
        public Crear()
        {
            _container = new Register().RegisterAllInstances();
            InitializeComponent();
        }
        public Crear(int inspeccionId)
        {
            _container = new Register().RegisterAllInstances();
            InitializeComponent();
            Init(inspeccionId);
        }
        private void LoadCbbKyeVaue(DevExpress.XtraEditors.ImageComboBoxEdit cbb, IEnumerable<IBaseEntity> list)
        {
            var items = cbb.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (dynamic record in list.ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(record.Nombre, value: record.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }
        void Init(int inspeccionId)
        {
            model = new Renta();
            _db = new BaseRespository<Renta>();
            _context = new RentCarContext();
           

            _inspeccion = _context.Inspeccion
                .Include(x=>x.Cliente)
                .Include(x=>x.Vehiculo)
                .Include(x=>x.Empleado)
                .FirstOrDefault(x=> x.Id == inspeccionId);

            LoadCbbKyeVaue(cbbCliente, _context.Cliente);
            LoadCbbKyeVaue(cbbEmpleado, _context.Empleado);

            model.FechaRenta = txtFechaRenta.DateTime = DateTime.Now;
            model.VehiculoId = _inspeccion.VehiculoId;
            lblPlacaVehiculo.Text = _inspeccion.Vehiculo.NoPlaca;
            cbbCliente.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_inspeccion.Cliente.Nombre, value: _inspeccion.ClienteId);
            cbbEmpleado.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_inspeccion.Empleado.Nombre, value: _inspeccion.EmpleadoId);

        }
        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                  
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }


        public Crear(Renta editingEntity)
        {
            model = new Renta();
            _context = new RentCarContext();
            _db = new BaseRespository<Renta>();
            _devolucionEntity = _context.Renta
                .Include(x=>x.Empleado)
                .Include(x => x.Cliente)
                .Include(x => x.Vehiculo)
                .FirstOrDefault(x=> x.Id == editingEntity.Id);

            InitializeComponent();
          
            LoadCbbKyeVaue(cbbCliente, _context.Cliente);
            LoadCbbKyeVaue(cbbEmpleado, _context.Empleado);



            txtFechaRenta.DateTime = _devolucionEntity.FechaRenta;
            txtFechaRenta.Enabled = false;

            txtMontoDia.Value = _devolucionEntity.MontoPorDia;
            txtMontoDia.Enabled = false;

            txtCantidadDias.Value = _devolucionEntity.CantidadDia;
            txtCantidadDias.Enabled = false;

            lblTitleRenta.Text = "Devolución del vehículo con placa";

            lblPlacaVehiculo.Text = _devolucionEntity.Vehiculo.NoPlaca;
            cbbCliente.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_devolucionEntity.Cliente.Nombre, value: _devolucionEntity.ClienteId);
            cbbCliente.Enabled = false;
            cbbEmpleado.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_devolucionEntity.Empleado.Nombre, value: _devolucionEntity.EmpleadoId);
            cbbEmpleado.Enabled = false;
        }

        void Save()
        {
            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            _container = new Register().RegisterAllInstances();
            var vehiculoService = _container.GetInstance<IVehiculoService>();

            if (_devolucionEntity != null)
            {
                _devolucionEntity.FechaDevolucion = txtFechaDevolucion.DateTime;
                _devolucionEntity.Comentario = txtComentario.Text;
                _devolucionEntity.Empleado = null;
                _devolucionEntity.Cliente = null;
                _devolucionEntity.Vehiculo = null;
                _devolucionEntity.Estado = "Devuelto";
                _db.Update(_devolucionEntity);
                _db.SaveChanges();
                vehiculoService.UnmarkAsRentado(model.VehiculoId);
            }
            else
            {
                if (vehiculoService.IsRentado(model.VehiculoId)){
                    MessageBox.Show("Este vehiculo esta Rentado actualmente");
                    return;
                }

                model.FechaRenta = txtFechaRenta.DateTime == null || txtFechaRenta.Text == ""? DateTime.Now : txtFechaRenta.DateTime;
                model.FechaDevolucion = txtFechaDevolucion.DateTime;
                model.Comentario = txtComentario.Text;
                model.MontoPorDia = txtMontoDia.Value;
                model.CantidadDia = (int)txtCantidadDias.Value;
                model.Estado = "Rentado";
                _db.Add(model);
                _db.SaveChanges();
                vehiculoService.MarkAsRentado(model.VehiculoId);
            }

            Limpiar();
            Regresar();
        }
        void Limpiar()
        {
            model = new Renta();
        }
        void Regresar()
        {
            Visible = false;
            new Home
            {
                Visible = true
            };
        }

        private void txtMontoDia_ValueChanged(object sender, System.EventArgs e)
        {
            var txt = (System.Windows.Forms.NumericUpDown)sender;
            if (txt != null)
            {
                var value = Decimal.Parse((txt.Value * txtCantidadDias.Value).ToString());
                lblTotal.Text = $"RD${value}";
            }
            else {
                lblTotal.Text = $"RD$0";
            }
           
        }

        private void txtCantidadDias_ValueChanged(object sender, System.EventArgs e)
        {
            var txt = (System.Windows.Forms.NumericUpDown)sender;
            if (txt != null)
            {
                var vaue = Decimal.Parse((txt.Value * txtMontoDia.Value).ToString());
                lblTotal.Text = $"RD${vaue}";
            }
            else
            {
                lblTotal.Text = $"RD$0";
            }
        }

        private void cbbCliente_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                Int32.TryParse(selected.ToString(), out int id);

                if (_devolucionEntity != null)
                {
                    _devolucionEntity.ClienteId = id;
                }
                else
                {
                    model.ClienteId = id;
                }
            }
        }

        private void cbbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                Int32.TryParse(selected.ToString(), out int id);

                if (_devolucionEntity != null)
                {
                    _devolucionEntity.EmpleadoId = id;
                }
                else
                {
                    model.EmpleadoId = id;
                }
            }
        }

        private void txtFechaRenta_EditValueChanged(object sender, EventArgs e)
        {
            txtFechaDevolucion.Properties.MinValue = txtFechaRenta.DateTime;
        }

        private void txtFechaDevolucion_EditValueChanged(object sender, EventArgs e)
        {
            txtFechaDevolucion.Properties.MinValue = txtFechaRenta.DateTime;
        }


        bool ModelIsValid()
        {
            if (cbbCliente.Text == null || cbbCliente.Text == "" ||
              cbbEmpleado.Text == null || cbbEmpleado.Text == "" ||
              txtMontoDia.Text == null || txtMontoDia.Text == "" ||
              txtCantidadDias.Text == null || txtCantidadDias.Text == "" 
             )
                return false;

            return true;
        }
    }
}
