﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using RentCar.Model.Models;
using RentCar.Model;
using Microsoft.EntityFrameworkCore;

namespace RentCar.Home
{
    public partial class Home : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        RentCarContext _context;
        ViewModel _modelSelected;
        public Home()
        {
            _context = new RentCarContext();
            InitializeComponent();
            LoadData();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<ViewModel> GetDataSource()
        {
            BindingList<ViewModel> result = new BindingList<ViewModel>();


            foreach (var item in _context.Renta
                .Include(x => x.Vehiculo)
                .Include(x => x.Cliente)
                .Include(x => x.Empleado)
                .ToList())
            {
                result.Add(new ViewModel
                {
                    Id = item.Id,
                    Estado = item.Estado,
                    Empleado = item.Empleado.Nombre,
                    Cliente = item.Cliente.Nombre,
                    Vehiculo = $"Placa: {item.Vehiculo.NoPlaca}",
                    CreatedDate = item.CreatedDate,
                    CantidadDias = item.CantidadDia,
                    Comentario = item.Comentario,
                    FechaDevolucion = item.FechaDevolucion,
                    Total = item.Total.ToString(),
                    FechaRenta = item.FechaRenta
                });
            }
            return result;
        }
        private void LoadData()
        {
            BindingList<ViewModel> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "Resgistros : " + dataSource.Count;
        }
        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            new TipoCombustibleForm.Lista
            {
                Visible = true
            };
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            new TipoVehiculoForm.Lista
            {
                Visible = true
            };
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            new MarcaForm.Lista
            {
                Visible = true
            };
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            new ModeloForm.Lista
            {
                Visible = true
            };
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            new VehiculoForm.Lista
            {
                Visible = true
            };
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            new ClienteForm.Lista
            {
                Visible = true
            };
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            new EmpleadoForm.Lista
            {
                Visible = true
            };
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            new InspeccionForm.Crear
            {
                Visible = true
            };
        }

       

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            new InspeccionForm.Lista
            {
                Visible = true
            };
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            _modelSelected = (ViewModel)row.GetFocusedRow();
        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(_modelSelected != null)
            {
                if (_modelSelected.Estado == "Rentado")
                {
                    new InspeccionForm.Crear(_modelSelected.Id).Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Seleccione una fila","Operacion invalida",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
    }

    public class ViewModel
    {
        [Display(Name = "No. de renda")]
        public int Id { get; set; }
        [Display(Name = "Vehículo")]
        public string Vehiculo { get; set; }
        public string Cliente { get; set; }
        public string Empleado { get; set; }
        [Display(Name = "Cantidad de dias")]
        public int CantidadDias { get; set; }
        [Display(Name = "Monto total")]
        public string Total { get; set; }
        [Display(Name = "Fecha de renta")]
        public DateTime FechaRenta { get; set; }
        [Display(Name = "Fecha de devolución")]
        public DateTime? FechaDevolucion { get; set; }
        public string Estado { get; set; }
        public string Comentario { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
    }
}