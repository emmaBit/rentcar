﻿using RentCar.BL.Factory;
using RentCar.BL.Repositories;
using RentCar.BL.Services;
using RentCar.Model;
using RentCar.Model.Models;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar
{
    static class Program
    {
        static readonly Container _container;
        static Program()
        {
            
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Inicio());
        }
    }
}
