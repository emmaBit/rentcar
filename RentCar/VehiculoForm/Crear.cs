﻿using RentCar.Model.Models;
using RentCar.BL.Repositories;
using DevExpress.XtraBars.Docking2010;
using System;
using RentCar.Model;
using System.Linq;
using System.Windows.Forms;

namespace RentCar.VehiculoForm
{
    public partial class Crear : DevExpress.XtraEditors.XtraForm
    {
        private Vehiculo model;
        private Vehiculo _editingEntity;
        private BaseRespository<Vehiculo> _db;
        private IRentCarContext _context;


        public Crear()
        {
            model = new Vehiculo();
            Init();
        }


        private void Init()
        {
            _db = new BaseRespository<Vehiculo>();
            _context = new RentCarContext();
            InitializeComponent();
            LoadMarcas();
            LoadTipoVehiculos();
            LoadTipoCombustible();
        }
        private void LoadMarcas()
        {
            var items = cbbMarcas.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (var marca in _context.Marca.ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(marca.Nombre, value: marca.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }
        private void LoadTipoVehiculos()
        {
            var items = cbbTipoVehiculos.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (var tipo in _context.TipoVehiculo.ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(tipo.Nombre, value: tipo.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }
        private void LoadTipoCombustible()
        {
            var items = cbbTipoCombustible.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (var tipo in _context.TipoCombustible.ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(tipo.Nombre, value: tipo.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }
        private void LoadModelos(int marcaId)
        {
            var items = cbbModelos.Properties.Items;
            items.BeginUpdate();
            try
            {
                foreach (var modelo in _context.Modelo.Where(x=> x.MarcaId == marcaId).ToList())
                {
                    items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(modelo.Nombre, value: modelo.Id));
                }
            }
            finally
            {
                items.EndUpdate();
            }
        }

        void onPanelBottomBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Guardar":
                    Save();
                   
                    break;
                case "Limpiar":
                    Limpiar();
                    break;
            }
        }
        void onPanelLeftBtnClick(object sender, ButtonEventArgs e)
        {
            string tag = ((WindowsUIButton)e.Button).Tag.ToString();
            switch (tag)
            {
                case "Regresar":
                    Regresar();
                    break;
            }
        }


        public Crear(Vehiculo editingEntity)
        {
            _editingEntity = editingEntity;
            Init();
            txtNoChasis.Text = _editingEntity.NoChasis;
            txtNoMotor.Text = _editingEntity.NoMotor;
            txtNoPlaca.Text = _editingEntity.NoPlaca;
            txtDescripcion.Text = _editingEntity.Descripcion;
            cbbMarcas.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(editingEntity.Modelo.Marca.Nombre, value: editingEntity.Modelo.MarcaId);
          
            cbbTipoVehiculos.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(editingEntity.TipoVehiculo.Nombre, value: editingEntity.TipoVehiculoId);
            cbbTipoCombustible.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(editingEntity.TipoCombustible.Nombre, value: editingEntity.TipoCombustibleId);
        }

        void Save()
        {
            if (!ModelIsValid())
            {
                MessageBox.Show("Datos faltantes o invalidos", "Operacion invalida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (_editingEntity != null)
            {
                _editingEntity.NoMotor = txtNoMotor.Text;
                _editingEntity.NoPlaca = txtNoPlaca.Text;
                _editingEntity.NoChasis = txtNoChasis.Text;
                _editingEntity.Descripcion = txtDescripcion.Text;

                _editingEntity.TipoCombustible = null;
                _editingEntity.TipoVehiculo = null;
                _editingEntity.Modelo = null;

                _db.Update(_editingEntity);
            }
            else
            {
                model.NoChasis = txtNoChasis.Text;
                model.NoPlaca = txtNoPlaca.Text;
                model.NoMotor = txtNoMotor.Text;
                model.Descripcion = txtDescripcion.Text;

                model.Estado = "Activo";
                _db.Add(model);
            }
            _db.SaveChanges();
            Limpiar();

            Regresar();
        }
        void Limpiar()
        {
            model = new Vehiculo();
            txtNoChasis.Text = model.NoChasis;
            txtNoMotor.Text = model.NoMotor;
            txtNoPlaca.Text = model.NoPlaca;
            txtDescripcion.Text = model.Descripcion;
            cbbMarcas.EditValue = null;
            cbbTipoVehiculos.EditValue = null;
            cbbTipoCombustible.EditValue = null;
        }
        void Regresar()
        {
            Visible = false;
            new Lista
            {
                Visible = true
            };
        }

        private void cbbMarcas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if(selected != null)
                {
                    Int32.TryParse(selected.ToString(), out int marcaId);
                    LoadModelos(marcaId);

                    if (_editingEntity != null)
                    {
                        cbbModelos.EditValue = new DevExpress.XtraEditors.Controls.ImageComboBoxItem(_editingEntity.Modelo.Nombre, value: _editingEntity.ModeloId);
                    }
                    cbbModelos.Enabled = true;
                }

            }
        }

        private void cbbModelos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                Int32.TryParse(selected.ToString(), out int modeloId);

                if (_editingEntity != null)
                {
                    _editingEntity.ModeloId = modeloId;
                }
                else
                {
                    model.ModeloId = modeloId;
                }
            }
        }

        private void cbbTipoVehiculos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if(selected != null)
                {

                    Int32.TryParse(selected.ToString(), out int id);
                    if (_editingEntity != null)
                    {
                        _editingEntity.TipoVehiculoId = id;
                    }
                    else
                    {
                        model.TipoVehiculoId = id;
                    }
                }
         

            }
        }

        private void cbbTipoCombustible_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var selected = ((DevExpress.XtraEditors.ImageComboBoxEdit)sender).EditValue;
                if(selected != null)
                {
                    Int32.TryParse(selected.ToString(), out int id);
                    if (_editingEntity != null)
                    {
                        _editingEntity.TipoCombustibleId = id;
                    }
                    else
                    {
                        model.TipoCombustibleId = id;
                    }
                }
                
            }
        }

        bool ModelIsValid()
        {
            if (cbbMarcas.Text == null || cbbMarcas.Text == "" ||
                cbbModelos.Text == null || cbbModelos.Text == "" ||
                txtNoChasis.Text == null || txtNoChasis.Text == "" ||
                txtNoMotor.Text == null || txtNoMotor.Text == "" ||
                txtNoPlaca.Text == null || txtNoPlaca.Text == "" ||
                cbbTipoCombustible.Text == null || cbbTipoCombustible.Text == "" ||
                cbbTipoVehiculos.Text == null || cbbTipoVehiculos.Text == ""
                )
                return false;

            return true;
        }
    }
}
