﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RentCar.BL.Repositories;
using RentCar.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using RentCar.Model.Models;

namespace RentCar.VehiculoForm
{
    public partial class Lista : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private IBaseRespository<Vehiculo> _db;
        RentCarContext _context;
        private ViewModel _modelSelected;
        public Lista()
        {
            _context = new RentCarContext();
            _db = new BaseRespository<Vehiculo>();
            InitializeComponent();
            LoadData();
        }

        public BindingList<ViewModel> GetDataSource()
        {
            BindingList<ViewModel> result = new BindingList<ViewModel>();


            foreach (var item in _context.Vehiculo
                .Include(x => x.TipoCombustible)
                .Include(x => x.TipoVehiculo)
                .Include(x => x.Modelo.Marca)
                .ToList())
            {
                result.Add(new ViewModel
                {
                    Id = item.Id,
                    Estado = item.Estado,
                    NoChasis = item.NoChasis,
                    NoMotor = item.NoMotor,
                    NoPlaca = item.NoPlaca,
                    Marca = item.Modelo?.Marca?.Nombre,
                    Modelo = item.Modelo?.Nombre,
                    TipoVehiculo = item.TipoVehiculo?.Nombre,
                    TipoCombustible = item.TipoCombustible?.Nombre,
                    Descripcion = item.Descripcion,
                    CreatedDate = item.CreatedDate
                });
            }
            return result;
        }
        private void LoadData()
        {
            BindingList<ViewModel> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "Resgistros : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Visible = false;

            var nuevoVehiculo = new Crear
            {
                Visible = true
            };
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            _modelSelected = (ViewModel)row.GetFocusedRow();
        }

        private void btnEditar_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                var obj = _context.Vehiculo
                    .Include(x => x.TipoCombustible)
                    .Include(x => x.TipoVehiculo)
                    .Include(x => x.Modelo.Marca)
                    .FirstOrDefault(x => x.Id == _modelSelected.Id);
                Visible = false;
                new Crear(obj).Visible = true;
            }
        }

        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_modelSelected != null)
            {
                var result = MessageBox.Show("¿Borrar este registro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    _db.Remove(_modelSelected.Id);
                    _db.SaveChanges();
                    LoadData();
                }
            }
        }
    }

    public class ViewModel
    {
        public ViewModel()
        {

        }
        public int Id { get; set; }
    
        public string Marca { get; set; }
        public string Modelo { get; set; }
        [Display(Name = "No. de chasis")]
        public string NoChasis { get; set; }
        [Display(Name = "No. de placa")]
        public string NoPlaca { get; set; }
        [Display(Name = "No. de motor")]
        public string NoMotor { get; set; }
        [Display(Name = "Tipo")]
        public string TipoVehiculo { get; set; }
        [Display(Name = "Tipo de combustible")]
        public string TipoCombustible { get; set; }
        public string Estado { get; set; }
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
    }
}
