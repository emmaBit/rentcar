﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
   public class Modelo : IBaseEntity
    {
        public Modelo()
        {
            Vehiculos = new HashSet<Vehiculo>();
        }
        [Column("ModeloId")]
        public int Id { get; set; }
        public int MarcaId { get; set; }
        public bool Deleted { get; set; }
        public string Estado { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual Marca Marca { get; set; }
        public virtual ICollection<Vehiculo> Vehiculos { get; set; }
    }
}
