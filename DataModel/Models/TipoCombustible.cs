﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class TipoCombustible : IBaseEntity
    {
        public TipoCombustible()
        {
            Vehiculos = new HashSet<Vehiculo>();
        }
        [Column("TipoCombustibleId")]
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public string Estado { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual ICollection<Vehiculo> Vehiculos { get; set; }
    }
   
}
