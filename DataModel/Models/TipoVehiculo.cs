﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class TipoVehiculo : IBaseEntity
    {
        public TipoVehiculo()
        {
            Vehiculos = new HashSet<Vehiculo>();
        }
        [Column("TipoVehiculoId")]
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public string Estado { get; set; }
        public string Nombre { get; set; }
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedDate { get; set; }
        public virtual ICollection<Vehiculo> Vehiculos { get; set; }
    }
}
