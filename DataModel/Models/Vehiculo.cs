﻿using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentCar.Model.Models
{
    public class Vehiculo : IBaseEntity
    {
        public Vehiculo()
        {
            Inspecciones = new HashSet<Inspeccion>();
            Rentas = new HashSet<Renta>();
        }
        [Column("VehiculoId")]
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string NoChasis { get; set; }
        public string NoPlaca { get; set; }
        public string NoMotor { get; set; }
        public int ModeloId { get; set; }
        public int TipoCombustibleId { get; set; }
        public int TipoVehiculoId { get; set; }
        public virtual Modelo Modelo { get; set; }
        public virtual TipoCombustible TipoCombustible { get; set; }
        public virtual TipoVehiculo TipoVehiculo { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<Inspeccion> Inspecciones { get; set; }
        public virtual ICollection<Renta> Rentas { get; set; }
    }
}
