﻿using System.ComponentModel;

namespace RentCar.Model.Enums
{
    public enum TipoPersona
    {
        [Description("Física")]
        Fisica = 0,
        [Description("Jurídica")]
        Juridica = 1
    }
}
