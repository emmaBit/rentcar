﻿using System.ComponentModel;

namespace RentCar.Model.Enums
{
    public enum CantidadCombustible
    {
        [Description("1/4")]
        UnCuarto = 0,
        [Description("1/2")]
        UnMedio = 1,
        [Description("3/4")]
        TresCuartos = 2,
        [Description("LLeno")]
        LLeno = 3,
        [Description("Otro")]
        Otro = 4
    }
}
