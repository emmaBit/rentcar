﻿using System.ComponentModel;

namespace RentCar.Model.Enums
{
    public enum EstadoGoma
    {
        [Description("Bueno")]
        Bueno = 0,
        [Description("Regular")]
        Regular = 1,
        [Description("Malo")]
        Malo = 2,
        [Description("No existe")]
        NoExiste = 3
    }
}
