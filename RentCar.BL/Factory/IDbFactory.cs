﻿using RentCar.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentCar.BL.Factory
{
    public interface IDbFactory
    {
        IRentCarContext GetRentCarContext();
    }

    public class DbFactory : IDbFactory
    {

        public IRentCarContext GetRentCarContext() {
            return new RentCarContext();
        }

    }
}
