﻿using Microsoft.EntityFrameworkCore;
using RentCar.BL.Factory;
using RentCar.Model;
using RentCar.Model.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RentCar.BL.Repositories
{
    public interface IBaseRespository<T>: IDisposable where T : class, IBaseEntity
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        T Add(T model);
        T Remove(int id);
        T Update(T model);
        void SaveChanges();
    }

    public class BaseRespository<T> : IBaseRespository<T> where T : class, IBaseEntity
    {
        private DbSet<T> _db;
        private IRentCarContext _ctx;
        //public BaseRespository()
        //{

        //}
        public BaseRespository()
        {
            _ctx = new DbFactory().GetRentCarContext();
            _db = _ctx.GetDbSet<T>();
        }

        public T Add(T model)
        {
          var result =  _db.Add(model);

          return result.Entity;
        }
        

        public T Get(int id)
        {
            var result = _db.FirstOrDefault(x=> x.Id == id);
            return result;
        }

        public IEnumerable<T> GetAll()
        {
            var result = _db.ToList();
            return result;
        }

        public T Remove(int id)
        {
            var result = Get(id);
            _db.Remove(result);
            return result;
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges(true);
        }

        public T Update(T model)
        {
            var result = _db.Update(model);
            return result.Entity;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _ctx.Dispose(disposing);
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
