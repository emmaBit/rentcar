﻿using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RentCar.BL.Services
{
    public interface IVehiculoService
    {
        void MarkAsRentado(int id);
        void UnmarkAsRentado(int id);
        bool IsRentado(int id);
    }

    public class VehiculoService : IVehiculoService
    {
        RentCarContext _context = new RentCarContext();
        public bool IsRentado(int id)
        {
            var model = _context.Vehiculo.AsNoTracking().FirstOrDefault(x => x.Id == id);
            return model?.Estado == "Rentado";
        }
        public void MarkAsRentado(int id)
        {
            var model = _context.Vehiculo.AsNoTracking().FirstOrDefault(x => x.Id == id);
            if(model != null)
            {
                model.Estado = "Rentado";
                _context.Vehiculo.Update(model);
                _context.SaveChanges();
            }
        }

        public void UnmarkAsRentado(int id)
        {
            var model = _context.Vehiculo.AsNoTracking().FirstOrDefault(x => x.Id == id);
            if (model != null)
            {
                model.Estado = "Activo";
                _context.Vehiculo.Update(model);
                _context.SaveChanges();
            }
        }
    }
}
